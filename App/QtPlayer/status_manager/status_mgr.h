﻿#ifndef STATUS_MGR_H
#define STATUS_MGR_H

#include <QObject>
#include <QString>

#include "AVThread/AVThread.h"

enum class PlayStatus {
	kUnknow,
	kPlay,
	kPause,
	kEnd,
	kError
};

enum class PlayMode {
	kSequence = 0,
	kRandom,
	kSingle
};

/**
 * 全局状态控制单例
 */
class StatusManager :public QObject {
	Q_OBJECT
public:
	static StatusManager* Instance();

	Q_PROPERTY(PlayStatus PlayState READ PlayState WRITE SetPlayState NOTIFY PlayStateChanged);
	Q_PROPERTY(PlayMode playMode READ playMode WRITE SetPlayMode NOTIFY PlayModeChanged);
	Q_PROPERTY(double volume READ Volume WRITE SetVolume NOTIFY VolumeChanged);
	Q_PROPERTY(int duration READ Duration WRITE SetDuration NOTIFY DurationChanged);
	Q_PROPERTY(double progress READ Progress WRITE SetProgress NOTIFY ProgressChanged);
	Q_PROPERTY(double speed READ Speed WRITE SetSpeed NOTIFY SpeedChanged);
	Q_PROPERTY(bool isReverse READ IsReverse WRITE SetIsReverse NOTIFY isReverseChanged);
	Q_PROPERTY(bool isFullScreen READ IsFullScreen WRITE SetIsFullScreen NOTIFY isFullScreenChanged);
    Q_PROPERTY(AV::VideoInfo videoInfo READ VideoInfo WRITE SetVideoInfo NOTIFY VideoInfoChanged);

	PlayStatus PlayState() const;
	void SetPlayState(PlayStatus newPlayState);

	void SendOpen(QString path);
	void SendForward(int second);
	void SendBackward(int second);

	double Volume() const;
	void SetVolume(double newVolume);

	int Duration() const;
	void SetDuration(int newDuration);

	int Progress() const;
	void SetProgress(double newProgress);

	double Speed() const;
	void SetSpeed(double newSpeed);

	const std::string& GetPath();

	bool IsReverse() const;
	void SetIsReverse(bool newIsReverse);

	bool IsFullScreen() const;
	void SetIsFullScreen(bool newIsFullScreen);

	const PlayMode& playMode() const;
	void SetPlayMode(const PlayMode& newPlayMode);

    const AV::VideoInfo &VideoInfo() const;
    void SetVideoInfo(const AV::VideoInfo &newVideoInfo);

signals:
	void PlayStateChanged();

	void VolumeChanged();

	void DurationChanged();

	void Open(QString path);

	void OpenSuccess();

	void ProgressChanged();

	void SpeedChanged();

	void Forward(int second);
	void Backward(int second);
	void isReverseChanged();

	void isFullScreenChanged();

	void PlayModeChanged();

    void VideoInfoChanged();

protected:
	StatusManager();

private:
	PlayStatus m_playState = PlayStatus::kUnknow;
	std::string m_videoPath;
	double m_volume = 100;
	int m_duration = 0;
	double m_progress = 0;
	double m_speed = 1;
	bool m_isReverse = false;
	bool m_isFullScreen = false;
    PlayMode m_playMode = PlayMode::kSequence;
    AV::VideoInfo m_videoInfo;
};

StatusManager* GetStatusMgr();

#endif
