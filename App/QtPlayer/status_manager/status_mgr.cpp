﻿#include"status_mgr.h"


StatusManager::StatusManager() :m_playState(PlayStatus::kUnknow)
{
}

StatusManager* StatusManager::Instance()
{
	static StatusManager* ins = new StatusManager();
	return ins;
}

PlayStatus StatusManager::PlayState() const
{
	return m_playState;
}

void StatusManager::SetPlayState(PlayStatus newPlayState)
{
	if (m_playState == newPlayState)
		return;
	m_playState = newPlayState;
	emit PlayStateChanged();
}

void StatusManager::SendOpen(QString path)
{
	m_videoPath = path.toStdString();
	emit Open(path);
}

void StatusManager::SendForward(int second)
{
	emit Forward(second);
}

void StatusManager::SendBackward(int second)
{
	emit Backward(second);
}

StatusManager* GetStatusMgr() {
	return StatusManager::Instance();
}

double StatusManager::Volume() const
{
	return m_volume;
}

void StatusManager::SetVolume(double newVolume)
{
	if (m_volume == newVolume)
		return;
	m_volume = newVolume;
	emit VolumeChanged();
}

int StatusManager::Duration() const
{
	return m_duration;
}

void StatusManager::SetDuration(int newDuration)
{
	if (m_duration == newDuration)
		return;
	m_duration = newDuration;
	emit DurationChanged();
}

int StatusManager::Progress() const
{
	return m_progress;
}

void StatusManager::SetProgress(double newProgress)
{
	if (m_progress == newProgress)
		return;
	m_progress = newProgress;
	emit ProgressChanged();
}

double StatusManager::Speed() const
{
	return m_speed;
}

void StatusManager::SetSpeed(double newSpeed)
{
	if (qFuzzyCompare(m_speed, newSpeed))
		return;
	m_speed = newSpeed;
	emit SpeedChanged();
}

const std::string& StatusManager::GetPath()
{
	return m_videoPath;
}

bool StatusManager::IsReverse() const
{
    return m_isReverse;
}

void StatusManager::SetIsReverse(bool newIsReverse)
{
    if (m_isReverse == newIsReverse)
        return;
    m_isReverse = newIsReverse;
    emit isReverseChanged();
}

bool StatusManager::IsFullScreen() const
{
    return m_isFullScreen;
}

void StatusManager::SetIsFullScreen(bool newIsFullScreen)
{
    if (m_isFullScreen == newIsFullScreen)
        return;
    m_isFullScreen = newIsFullScreen;
    emit isFullScreenChanged();
}

const PlayMode &StatusManager::playMode() const
{
    return m_playMode;
}

void StatusManager::SetPlayMode(const enum PlayMode &newPlayMode)
{
    if (m_playMode == newPlayMode)
        return;
	m_playMode = newPlayMode;
    emit PlayModeChanged();
}

const AV::VideoInfo &StatusManager::VideoInfo() const
{
    return m_videoInfo;
}

void StatusManager::SetVideoInfo(const AV::VideoInfo &newVideoInfo)
{
    if (m_videoInfo == newVideoInfo)
        return;
    m_videoInfo = newVideoInfo;
    emit VideoInfoChanged();
}
