﻿#include "sdlchild.h"

#include <QApplication>
#include <QBoxLayout>
#include <QLabel>
#include <QRect>
#include <QSlider>
#include <QString>
#include <QTextBlock>
#include <QTextFrame>

#include "mywidget.h"

#include "status_manager/status_mgr.h"

SDLChild::SDLChild(QWidget* parent) : QMdiSubWindow(parent), m_player(nullptr) {
	this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    setWidget(new MyWidget());
	this->widget()->setUpdatesEnabled(false);

	SetupSigSlot();

}

SDLChild::~SDLChild() {}

void SDLChild::SDLInit() {
}

void SDLChild::enterEvent(QEnterEvent* event) {}

void SDLChild::leaveEvent(QEvent* leaveEvent) {}

void SDLChild::mouseDoubleClickEvent(QMouseEvent* event) {
	qDebug() << "mouseDoubleClickEvent";
	emit FullScreen();
	QMdiSubWindow::mouseDoubleClickEvent(event);
}

void SDLChild::resizeEvent(QResizeEvent* resizeEvent)
{
	QMdiSubWindow::resizeEvent(resizeEvent);
	//m_player->ResetWindow(this->pos().x(),this->pos().y(),width(),height());
}

void SDLChild::mouseMoveEvent(QMouseEvent* event)
{
	qDebug() << "mouse move";
}

void SDLChild::SetupSigSlot()
{
	//响应状态变化
	connect(GetStatusMgr(), &StatusManager::PlayStateChanged, this, [this]() {
		if (GetStatusMgr()->PlayState() == PlayStatus::kUnknow) {
			this->SDLPlay();
		}
		else if (GetStatusMgr()->PlayState() == PlayStatus::kPlay) {
			this->SDLPlay();
		}
		else if (GetStatusMgr()->PlayState() == PlayStatus::kPause) {
			this->SDLPause();
		}
		else {//错误管理

		}
		});

	//打开新视频
	connect(GetStatusMgr(), &StatusManager::Open, this, [this](QString path) {
		this->SDLOpen(path);
		});

	//调整音量
	connect(GetStatusMgr(), &StatusManager::VolumeChanged, this, [this]() {
		if (!m_player) return;
		m_player->SetVolume(GetStatusMgr()->Volume()/10);
		});
	//倍速
	connect(GetStatusMgr(), &StatusManager::SpeedChanged, this, &SDLChild::SDLSpeedChanged);

	//前进后退n秒
	connect(GetStatusMgr(), &StatusManager::Forward, this, &SDLChild::SDLForward);
	connect(GetStatusMgr(), &StatusManager::Backward, this, &SDLChild::SDLBackward);

    //倒放
    connect(GetStatusMgr(), &StatusManager::isReverseChanged, this, &SDLChild::SDLReverse);
}

void SDLChild::SDLPlay()
{
	if (nullptr == m_player) {
		GetStatusMgr()->SetPlayState(PlayStatus::kError);
		return;
	}
	GetStatusMgr()->SetPlayState(PlayStatus::kPlay);
	m_player->Play();

}

void SDLChild::SDLPause()
{
	if (nullptr == m_player) {
		GetStatusMgr()->SetPlayState(PlayStatus::kError);
		return;
	}
	GetStatusMgr()->SetPlayState(PlayStatus::kPause);
	m_player->Pause();
}

void SDLChild::SDLOpen(QString path)
{
	if (path.isEmpty()) {
		return;
	}
	if (m_player) {//之前已经创建过，需要重新创建新窗口
		delete m_player;
		m_player = nullptr;
	}
	m_player = new AV::MediaPlayer();
	qDebug() << "WinID:" << widget()->winId();
	m_player->Open(path.toStdString(), (void*)widget()->winId());
	//自动播放
	m_player->Play();
	GetStatusMgr()->SetPlayState(PlayStatus::kPlay);
	auto info = m_player->GetVideoInfo();
	GetStatusMgr()->SetDuration(atoi(info["duration"].c_str()));
	qDebug() << "SDLOpen";
    GetStatusMgr()->SetVideoInfo(m_player->GetVideoInfo());
}

void SDLChild::SDLSpeedChanged()
{
	if (!m_player) {
		return;
	}
	m_player->SetSpeed(GetStatusMgr()->Speed());
}

void SDLChild::SDLSeek(double progress)
{
	if (!m_player) {
		return;
	}
	if (progress < 0 || progress>1) {
		return;
	}
	m_player->Seek(progress);
	qDebug() << "SDLSeek: expect time:" << progress * GetStatusMgr()->Duration();
    emit GetStatusMgr()->PlayStateChanged();
}

void SDLChild::SDLForward(double second)
{
	if (!m_player) {
		return;
	}
	double curProgress = GetStatusMgr()->Progress();
	double curPecent = curProgress / GetStatusMgr()->Duration();
	double newProgress = curProgress + second;
	double newPecent = newProgress / GetStatusMgr()->Duration();
	SDLSeek(newPecent > 1 ? 1 : newPecent);
}

void SDLChild::SDLBackward(double second)
{
	if (!m_player) {
		return;
	}
	double curProgress = GetStatusMgr()->Progress();
	double newProgress = curProgress - second;
	double newPecent = newProgress / GetStatusMgr()->Duration();
    SDLSeek(newPecent < 0 ? 0 : newPecent);
}

void SDLChild::SDLReverse()
{
	//m_player->Pause();
    m_player->Reverse();
	// if GetStatusMgr()->??() 
	//m_player->Play();
}

void SDLChild::UpdateLayout() {}
