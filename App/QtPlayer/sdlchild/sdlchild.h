﻿#ifndef _SDL_CHILD_H
#define _SDL_CHILD_H

#include <qmdisubwindow.h>
#include <qwidget.h>
#include <qtimer.h>
#include <QSlider>
#include <qgridlayout.h>
#include <SDL.h>
#include <QLabel>

#include "../Kernel/AVPlayer/AVPlayer.h"

class SDLChild : public QMdiSubWindow {
	Q_OBJECT
public:
	SDLChild(QWidget* parent);
	~SDLChild();

	/**
	 * @brief 创建SDLwindow和renderer
	 */
	void SDLInit();

	/**
	 * @brief 调整窗口宽高布局
	 */
	void UpdateLayout();

protected:
	void enterEvent(QEnterEvent* event) override;
	void leaveEvent(QEvent* event) override;
	void mouseDoubleClickEvent(QMouseEvent* event) override;
	void resizeEvent(QResizeEvent* resizeEvent) override;
	void mouseMoveEvent(QMouseEvent* event) override;

private:
	void SetupSigSlot();

signals:
	void FullScreen();

public slots:
	void SDLPlay();
	void SDLPause();
	void SDLOpen(QString path = "");
	void SDLSpeedChanged();
	/**
	 * @brief SDLSeek
	 * @param progress 进度条百分比
	 */
	void SDLSeek(double progress);
	void SDLForward(double second);
	void SDLBackward(double second);
    void SDLReverse();

private:
	AV::MediaPlayer* m_player;
};

#endif
