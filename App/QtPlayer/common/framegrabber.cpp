﻿#include "framegrabber.h"
#include <QVideoSink>
#include <QVideoFrame>
#include <QMediaPlayer>
#include <QDebug>
#include <QUrl>

FrameGrabber::FrameGrabber(QObject* parent)
	: QObject{ parent }
{
	qDebug() << "construct grabber.";
}
FrameGrabber::~FrameGrabber() {
	if (m_player) {
		m_player->stop();
	}
	qDebug() << "deconstruct grabber.";
}
bool FrameGrabber::Grab(QUrl url)
{
	m_player = new QMediaPlayer(nullptr);
	m_player->setSource(url);
	if (!m_player->isAvailable()) {
		return false;
	}
	if (m_player->mediaStatus() == QMediaPlayer::InvalidMedia) {
		qDebug() << "grab Invalid Media";
		return false;
	}
	this->BeginFetchVideoFrame();
	qDebug() << "begin frame grab." << url;
	return true;
}

void FrameGrabber::Stop()
{

	m_player->stop();
	qDebug() << "stop frame grab.";
}

void FrameGrabber::BeginFetchVideoFrame()
{
	QVideoSink* sink = new QVideoSink(m_player);
	connect(m_player, &QMediaPlayer::mediaStatusChanged, this, [=](QMediaPlayer::MediaStatus status) {
		if (status == QMediaPlayer::InvalidMedia) {
			qDebug("invalid media");
			emit Error();
		}
		});
	connect(sink, &QVideoSink::videoFrameChanged, this, [=](const QVideoFrame& frame) {
		if (!m_firstFlag) return;
		m_firstFlag = false;
		if (frame.isValid()) {
			QVideoFrame cloneFrame(frame);
			if (cloneFrame.map(QVideoFrame::ReadOnly))
			{
				QImage image = cloneFrame.toImage();
				//                const QPixmap& pixmap = QPixmap::fromImage(image);
				qDebug() << "grab frame!";
				emit VideoFrameFetched(image);
				cloneFrame.unmap();
			}
			else
			{
				qDebug("frame could not be mapped");
			}
		}
		else
		{
			qDebug("frame invalid!");
		}
		});
	m_player->setVideoSink(sink);
	m_player->play();
}


