﻿#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>

class MyWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MyWidget(QWidget *parent = nullptr);
    void mouseMoveEvent(QMouseEvent* e) override;

signals:
    void mouseMove(QMouseEvent* e);

};

#endif // MYWIDGET_H
