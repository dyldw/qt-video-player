﻿#ifndef MYSLIDER_H
#define MYSLIDER_H

#include <QObject>
#include <QSlider>
#include <QLabel>

class MySlider : public QSlider
{
	Q_OBJECT
public:
	explicit MySlider(QWidget* parent = nullptr);

	/**
	 * @brief MySlider
	 * @param orientation 横/纵向
	 * @param parent
	 */
	explicit MySlider(Qt::Orientation orientation, QWidget* parent = nullptr);

signals:
	void Preview(int time);
	void PreviewEnd();
	void Click(double progress);

protected:
	void mouseMoveEvent(QMouseEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void enterEvent(QEnterEvent* event) override;
	void leaveEvent(QEvent* event) override;

private:
	bool m_isEntry = false;

};

#endif // MYSLIDER_H
