﻿#include "fmp_frame_grabber.h"

#include "utili.h"


bool FmpFrameGrabber::is_init = false;
FmpFrameGrabber* FmpFrameGrabber::ins = nullptr;
QThread* FmpFrameGrabber::grabber_thread = nullptr;


const FmpFrameGrabber* FmpFrameGrabber::Instance()
{
	if (!is_init) {
		ins = new FmpFrameGrabber();
		grabber_thread = new QThread();
		if (!grabber_thread) {
			ins->deleteLater();
			return nullptr;
		}
		ins->moveToThread(grabber_thread);
		is_init = true;
	}
	return ins;
}

QImage FmpFrameGrabber::GetCover(const QString& video_path, const QString& save_path) const
{
	return GetFrame(video_path, "0", save_path);
}

QImage FmpFrameGrabber::GetFrame(const QString& video_path, const QString& time, const QString& save_path) const
{
	QString cmd = "-i " + video_path + " -frames:v 1 -ss " + time + " " + save_path + " -y";

	QByteArray cdata = cmd.toLocal8Bit();
	std::string cstr = std::string(cdata);

	Utili::MySystem("ffmpeg", cmd);

	QImage tmp(save_path);
	return tmp;
}

FmpFrameGrabber::FmpFrameGrabber()
{

}


void FmpFrameGrabber::Destroy()
{
	if (!is_init) {
		return;
	}
	ins->deleteLater();
	grabber_thread->deleteLater();
	is_init = false;
	return;
}


