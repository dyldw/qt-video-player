﻿#include "utili.h"
#include <QString>

LPCWSTR Utili::StringToLPCWSTR(std::string orig)
{
    wchar_t* wcstring = 0;
    try
    {
        size_t origsize = orig.length() + 1;
        const size_t newsize = 100;
        size_t convertedChars = 0;
        if (orig == "")
        {
            wcstring = (wchar_t*)malloc(0);
            mbstowcs_s(&convertedChars, wcstring, origsize, orig.c_str(), _TRUNCATE);
        }
        else
        {
            wcstring = (wchar_t*)malloc(sizeof(wchar_t) * (orig.length() - 1));
            mbstowcs_s(&convertedChars, wcstring, origsize, orig.c_str(), _TRUNCATE);
        }
    }
    catch (std::exception e)
    {
    }
    return wcstring;
}

QString Utili::timeItoS(int time)
{
    int h = time / 3600;
    int m = (time % 3600) / 60;
    int s = time % 60;
    QString hh = (h<10 ? "0"+QString::number(h):QString::number(h));
    QString mm = (m<10 ? "0"+QString::number(m):QString::number(m));
    QString ss = (s<10 ? "0"+QString::number(s):QString::number(s));
    if(h){ //h为0时省略
        return hh+":"+mm+":"+ss;
    }
    return mm+":"+ss;
}


void Utili::MySystem(const QString& cmd, const QString& par)
{
    std::wstring wCmd = cmd.toStdWString();
    std::wstring wPar = par.toStdWString();

    SHELLEXECUTEINFO ShExecInfo = { 0 };
    ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
    ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
    ShExecInfo.hwnd = NULL;
    ShExecInfo.lpVerb = NULL;
    ShExecInfo.lpFile = wCmd.c_str();//(LPCWSTR) cmd.c_str();//调用的程序名
    ShExecInfo.lpParameters = wPar.c_str();//(LPCWSTR)par.c_str();//调用程序的命令行参数
    ShExecInfo.lpDirectory = NULL;
    ShExecInfo.nShow = SW_HIDE;//窗口状态为隐藏
    ShExecInfo.hInstApp = NULL;
    ShellExecuteEx(&ShExecInfo); //启动新的程序
    WaitForSingleObject(ShExecInfo.hProcess, INFINITE);////等到该进程结束
}
