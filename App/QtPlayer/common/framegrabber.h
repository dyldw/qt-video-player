﻿#ifndef FRAMEGRABBER_H
#define FRAMEGRABBER_H

#include <QObject>

QT_BEGIN_NAMESPACE
class QMediaPlayer;
QT_END_NAMESPACE

class FrameGrabber : public QObject
{
	Q_OBJECT
public:
	explicit FrameGrabber(QObject* parent = nullptr);
	~FrameGrabber();
	bool Grab(QUrl url);
	void Stop();

signals:
	void Error();
	void VideoFrameFetched(const QImage& image);

private slots:


private:
	void BeginFetchVideoFrame();

	QMediaPlayer* m_player;
	bool m_firstFlag = true;
};

#endif // FRAMEGRABBER_H
