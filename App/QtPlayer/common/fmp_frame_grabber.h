﻿#ifndef FRAME_GRABBER_H
#define FRAME_GRABBER_H

#include <QObject>
#include <QThread>
#include <QImage>
#include <string>

class FmpFrameGrabber :public QObject
{
	Q_OBJECT
public:
	static bool is_init;
	static FmpFrameGrabber* ins;
	static QThread* grabber_thread;

	static const FmpFrameGrabber* Instance();

	QImage GetCover(const QString& video_path, const QString& save_path) const;

	QImage GetFrame(const QString& video_path, const QString& time, const QString& save_path) const;

	void Destroy();


protected:
	FmpFrameGrabber();

};

#endif // FRAME_GRABBER_H
