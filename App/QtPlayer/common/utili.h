﻿#ifndef UTILI_H
#define UTILI_H

#include <Windows.h>
#include <string>
#include <QString>

class Utili
{
public:
	static void MySystem(const QString& cmd, const QString& par);
	static LPCWSTR StringToLPCWSTR(std::string orig);
    static QString timeItoS(int time);
};

#endif // UTILI_H
