﻿#include "myslider.h"
#include <QMouseEvent>
#include <QFile>

MySlider::MySlider(QWidget* parent) : QSlider(parent)
{
	//加载样式
	QFile sliderStyle(":/SliderStyle.qss");
	sliderStyle.open(QFile::ReadOnly);
	if (sliderStyle.isOpen()) {
		qDebug() << "load sliderStyle";
		QString styleSheet = this->styleSheet();
		styleSheet += QString(sliderStyle.readAll());
		this->setStyleSheet(styleSheet);
	}

}

MySlider::MySlider(Qt::Orientation orientation, QWidget* parent) :QSlider(orientation, parent)
{
	//加载样式
	QFile sliderStyle(":/SliderStyle.qss");
	sliderStyle.open(QFile::ReadOnly);
	if (sliderStyle.isOpen()) {
		qDebug() << "load sliderStyle";
		QString styleSheet = this->styleSheet();
		styleSheet += QString(sliderStyle.readAll());
		this->setStyleSheet(styleSheet);
	}
}

void MySlider::mouseMoveEvent(QMouseEvent* event)
{

	if (!m_isEntry) {
		return;
	}
	int currentX = event->pos().x();
	double per = currentX * 1.0 / this->width();
	int value = per * (this->maximum() - this->minimum()) + this->minimum();
	emit PreviewEnd();
	emit Preview(value);
	QSlider::mouseMoveEvent(event);
}

void MySlider::mousePressEvent(QMouseEvent* event)
{
	int currentX = event->pos().x();
	double per = currentX * 1.0 / this->width();
	emit Click(per);
	int value = per * (this->maximum() - this->minimum()) + this->minimum();
	qDebug() << value;
	this->setValue(value);
	QSlider::mousePressEvent(event);
}

void MySlider::enterEvent(QEnterEvent* event)
{
	m_isEntry = true;
	QSlider::enterEvent(event);
}

void MySlider::leaveEvent(QEvent* event)
{
	m_isEntry = false;
	emit PreviewEnd();
	QSlider::leaveEvent(event);
}
