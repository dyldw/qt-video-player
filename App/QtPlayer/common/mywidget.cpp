﻿#include "mywidget.h"

#include <QDebug>

MyWidget::MyWidget(QWidget *parent)
    : QWidget{parent}
{
    setMouseTracking(true);
}

void MyWidget::mouseMoveEvent(QMouseEvent *e)
{
    qDebug()<<"mouse move";
    emit mouseMove(e);
}
