﻿#include "controlbar.h"

#include <QStyle>
#include <QApplication>
#include <QFile>

#include "status_mgr.h"
#include "Comm/MessageBus.hpp"
#include "utili.h"


ControlBar::ControlBar(QWidget* parent) :QMdiSubWindow(parent)
{
	this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
	setWidget(new QWidget);
	//计时器
	m_renderTimer = new QTimer(this);
	m_renderTimer->start(1000 / 60);
	//顶层
	m_mainLayout = new QVBoxLayout(this);
	//上方控件
	m_topLayout = new QHBoxLayout(this);
	m_progressSlider = new MySlider(Qt::Horizontal, this);
	m_progressLabel = new QLabel("00:00/00:00", this);
	m_botLayout = new QHBoxLayout(this);
	m_volumeBtn = new QPushButton(this);
	m_volumeBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaVolume));
	m_volumeSlider = new MySlider(Qt::Horizontal, this);
	m_volumeSlider->setMaximum(200);
	m_volumeSlider->setValue(m_volume);

	//下方控件
	//按钮
	m_playBtn = new QPushButton(this);
	m_playBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaPlay));
	m_playBtn->setFixedSize(QSize(35, 35));
	m_playBtn->setIconSize(QSize(25, 25));

	m_nextBtn = new QPushButton(this);
	m_nextBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaSkipForward));
	m_nextBtn->setFixedSize(QSize(35, 30));
	m_nextBtn->setIconSize(QSize(20, 15));

	m_prevBtn = new QPushButton(this);
	m_prevBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaSkipBackward));
	m_prevBtn->setFixedSize(QSize(35, 30));
	m_prevBtn->setIconSize(QSize(20, 15));

	m_forwardBtn = new QPushButton(this);
	m_forwardBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaSeekForward));
	m_forwardBtn->setFixedSize(QSize(35, 30));
	m_forwardBtn->setIconSize(QSize(20, 15));

	m_backwardBtn = new QPushButton(this);
	m_backwardBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaSeekBackward));
	m_backwardBtn->setFixedSize(QSize(35, 30));
	m_backwardBtn->setIconSize(QSize(20, 15));

    m_reverseBtn = new QPushButton(this);
    m_reverseBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaSeekBackward));
    m_reverseBtn->setFixedSize(QSize(35, 30));
    m_reverseBtn->setIconSize(QSize(20, 15));


    //播放选择
    m_playModeBox = new QComboBox(this);
    QStringList playMode = { "顺序播放","随机播放","单个循环" };
    m_playModeBox->addItems(playMode);

	//倍速选择
	m_speedBox = new QComboBox(this);
	QStringList speeds = { "x1.0","x1.25","x1.5","x2.0","x4.0" };
	m_speedBox->addItems(speeds);

	SetupLayout();
	SetupSigSlot();
}

ControlBar::~ControlBar()
{
	// 防止析构之后消息总线继续访问该指针
	MessageBus::Detach<uint64_t>("play progress");
}

void ControlBar::UpdateLayout()
{
	m_mainLayout->setGeometry(QRect(0, 0, width(), height()));
}

void ControlBar::SetValue(int val)
{
	m_progressSlider->setValue(val);
}

MySlider* ControlBar::GetProgressSlider() const
{
	return m_progressSlider;
}

QPushButton* ControlBar::GetPlayBtn() const
{
	return m_playBtn;
}

QPushButton* ControlBar::GetPrevBtn() const
{
	return m_prevBtn;
}

QPushButton* ControlBar::GetNextBtn() const
{
	return m_nextBtn;
}

void ControlBar::SetupLayout()
{
	// 上方布局
	m_progressSlider->setMaximum(300);
	m_progressSlider->setTracking(true);//允许拖动鼠标
	m_topLayout->addStretch();
	m_topLayout->addWidget(m_progressSlider, 16);
	m_topLayout->addSpacing(10);
    m_topLayout->addWidget(m_progressLabel, 2);
	m_topLayout->addWidget(m_volumeBtn, 1);
	m_topLayout->addSpacing(1);
	m_topLayout->addWidget(m_volumeSlider, 3);

	//下方布局
	m_botLayout->addStretch(1);
	m_botLayout->addWidget(m_prevBtn, 1);
	m_botLayout->addWidget(m_backwardBtn, 1);
	m_botLayout->addWidget(m_playBtn, 1);
	m_botLayout->addWidget(m_forwardBtn, 1);
	m_botLayout->addWidget(m_nextBtn, 1);
    m_botLayout->addStretch(4);
    m_botLayout->addWidget(m_reverseBtn,1);
    m_botLayout->addStretch(4);
    m_botLayout->addWidget(m_playModeBox,1);
	m_botLayout->addWidget(m_speedBox, 1);

	//顶层布局
	m_mainLayout->addLayout(m_topLayout, 1);
	m_mainLayout->addLayout(m_botLayout, 2);
}


QTimer* ControlBar::GetRenderTimer() const
{
	return m_renderTimer;
}


void ControlBar::SetupSigSlot()
{
	//刷新帧
	connect(m_renderTimer, &QTimer::timeout, this, [this]() {
		//OnFrameUpdate();
		emit FrameUpdate();
		});
	//静音
	connect(m_volumeBtn, &QPushButton::clicked, m_volumeBtn, [this]() {
		if (this->m_isMuted) {//当前静音
			m_volumeBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaVolume));
			m_volumeSlider->setValue(m_volume);
			m_isMuted = false;
		}
		else {//静音
			m_volumeBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaVolumeMuted));
			m_volumeSlider->setValue(0);
			m_isMuted = true;
		}
		qDebug() << "onVolumeBtn Clicked" << m_isMuted << " " << m_volume;
		});

	//音量条控制音量
	connect(m_volumeSlider, &QSlider::valueChanged, this, [this](int volume) {
		if (volume != 0) {
			m_isMuted = false;
			m_volume = volume;
			GetStatusMgr()->SetVolume(m_volume);
			m_volumeBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaVolume));

		}
		else {
			m_isMuted = true;
			GetStatusMgr()->SetVolume(0);
			m_volumeBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaVolumeMuted));
		}
		});
	// 全局音量控制
	connect(GetStatusMgr(), &StatusManager::VolumeChanged, this, [this]() {
		if (GetStatusMgr()->Volume() == m_volume) {
			return;
		}
		m_volumeSlider->setValue(GetStatusMgr()->Volume());
		});
	//播放/暂停
	connect(m_playBtn, &QPushButton::clicked, this, [this]() {
		if (GetStatusMgr()->PlayState() == PlayStatus::kPause) {
			GetStatusMgr()->SetPlayState(PlayStatus::kPlay);
		}
		else if (GetStatusMgr()->PlayState() == PlayStatus::kPlay) {
			GetStatusMgr()->SetPlayState(PlayStatus::kPause);
		}
		else {//错误管理
		}
		});
	// 根据播放暂停控制播放图标
	connect(GetStatusMgr(), &StatusManager::PlayStateChanged, this, [this]() {
		if (GetStatusMgr()->PlayState() == PlayStatus::kPause) {
			m_playBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaPlay));

		}
		else if (GetStatusMgr()->PlayState() == PlayStatus::kPlay) {
			m_playBtn->setIcon(QApplication::style()->standardIcon(QStyle::StandardPixmap::SP_MediaPause));
		}
		else {//错误管理
		}
		});

	// 进度条总时长变化
	connect(GetStatusMgr(), &StatusManager::DurationChanged, this, &ControlBar::OnDurationChanged);

    //播放模式
    connect(m_playModeBox, &QComboBox::currentTextChanged, this, &ControlBar::OnSelectPlayMode);

	//倍速
	connect(m_speedBox, &QComboBox::currentTextChanged, this, &ControlBar::OnSelectSpeed);
	

	connect(m_forwardBtn, &QPushButton::clicked, this, []() {
		GetStatusMgr()->SendForward(10);
		});
	connect(m_backwardBtn, &QPushButton::clicked, this, []() {
		GetStatusMgr()->SendBackward(10);
		});

    //倒放
    connect(m_reverseBtn, &QPushButton::clicked, this, []() {
        GetStatusMgr()->SetIsReverse(!(GetStatusMgr()->IsReverse()));
        });

	MessageBus::Attach("play progress", &ControlBar::UpdateProgress, this);
}

void ControlBar::OnDurationChanged()
{
	this->m_duration = GetStatusMgr()->Duration();
	m_progressSlider->setMaximum(m_duration);

	int cur_value = GetStatusMgr()->Progress();
	m_progressSlider->setValue(cur_value);
    GetStatusMgr()->SetProgress(cur_value);
    m_progressLabel->setText(Utili::timeItoS(cur_value)+ "/" + Utili::timeItoS(m_duration));

}

void ControlBar::OnSelectSpeed(const QString& text)
{
	if (text.isEmpty()) {
		return;
	}
	std::string tmp = text.toStdString();
	tmp = tmp.substr(1);
	double speed = atof(tmp.c_str());
	GetStatusMgr()->SetSpeed(speed);
    return;
}

void ControlBar::OnSelectPlayMode(const QString &text)
{
    if (text.isEmpty()) {
        return;
    }
    if(text=="顺序播放"){
        GetStatusMgr()->SetPlayMode(PlayMode::kSequence);
    }
    else if(text=="随机播放"){
            GetStatusMgr()->SetPlayMode(PlayMode::kRandom);
    }
    else if(text=="单个循环"){
            GetStatusMgr()->SetPlayMode(PlayMode::kSingle);
    }
}

void ControlBar::UpdateProgress(uint64_t progress)
{
	GetStatusMgr()->SetProgress(double(progress) / 1000.0);
	if (!m_progressSlider || !m_progressLabel) {
		return;
	}
    if(m_progressSlider->value() == progress / 1000){// 每秒刷新一次
        return;
    }
	m_progressSlider->setValue(progress / 1000);
    m_progressLabel->setText(Utili::timeItoS(progress / 1000)+ "/" + Utili::timeItoS(m_duration));
}

