﻿#ifndef CONTROLBAR_H
#define CONTROLBAR_H

#include <QObject>
#include <QMdiSubWindow>
#include <QSlider>
#include <QBoxLayout>
#include <QLabel>
#include <QTimer>
#include <QIcon>
#include <QPushButton>
#include <QComboBox>

#include "common/myslider.h"

class ControlBar :public QMdiSubWindow
{
	Q_OBJECT
public:
	ControlBar(QWidget* parent);
	~ControlBar();
	/**
	 * @brief 重新调整布局
	 */
	void UpdateLayout();

	/**
	 * @brief 更新进度条、显示文本的进度
	 * @param val 单位s
	 */
	void SetValue(int val);


	MySlider* GetProgressSlider() const;
	QPushButton* GetPlayBtn() const;
	QPushButton* GetPrevBtn() const;
	QPushButton* GetNextBtn() const;

	/**
	 * @brief 建立布局
	 */
	void SetupLayout();

	QTimer* GetRenderTimer() const;

	void UpdateProgress(uint64_t progress);

signals:
	void FrameUpdate();
	void Mute();
	void Unmute();
	void Play();
	void Pause();
public slots:
	void OnDurationChanged();

	void OnSelectSpeed(const QString& text);

    void OnSelectPlayMode(const QString& text);

private:
	void SetupSigSlot();
private:
	//布局
	QVBoxLayout* m_mainLayout;    /// 总体布局
	QHBoxLayout* m_topLayout;    /// 上方布局
	QHBoxLayout* m_botLayout;    /// 下方布局
	//进度
	MySlider* m_progressSlider;    /// 进度条
	QLabel* m_progressLabel;    /// 进度文本
	//预览
	QTimer* m_previewTimer;    /// 预览计时器
	//音量
	QPushButton* m_volumeBtn;    /// 音量控制按钮
	MySlider* m_volumeSlider;    /// 音量控制条
	//播放
	QPushButton* m_playBtn;/// 播放按钮
	QPushButton* m_nextBtn;/// 下一视频按钮
	QPushButton* m_prevBtn;/// 上一视频按钮
	QPushButton* m_forwardBtn;/// 前进5s按钮
	QPushButton* m_backwardBtn;/// 后退5s按钮

    QPushButton* m_reverseBtn;/// 倒放按钮

    QComboBox* m_playModeBox;
    QComboBox* m_speedBox;

	//待移交状态管理
	bool m_isMuted = false;    /// 是否静音
	int m_volume = 100;    /// 音量
	bool m_isPlaying = false;
	// 总时长、播放时长
	int m_duration = 0;
	int m_progress = 0;

	//Debug用的计时器
	QTimer* m_renderTimer;
};

#endif // CONTROLBAR_H
