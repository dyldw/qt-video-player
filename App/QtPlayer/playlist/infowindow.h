﻿#ifndef INFOWINDOW_H
#define INFOWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QLayout>
#include <QBoxLayout>

#include "AVThread/AVThread.h"

class InfoWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit InfoWindow(QWidget *parent = nullptr);
    void UpdateInfo();
private:
    void SetupLayout();
signals:

private:
    QVBoxLayout *m_rowLayout;
    /// 文件名
    QLabel* m_filePathLabel;
    QLineEdit* m_filePathEdit;
    /// 长宽
    QLabel* m_sizeLabel;
    QLineEdit* m_sizeEdit;
    /// 帧率
    QLabel* m_frameRateLabel;
    QLineEdit* m_frameRateEdit;
    /// 码率
    QLabel* m_bitRateLabel;
    QLineEdit* m_bitRateEdit;
    /// 视频格式
    QLabel* m_videoFormatLabel;
    QLineEdit* m_videoFormatEdit;

    /// 音频码率
    QLabel* m_audioSampleRateLabel;
    QLineEdit* m_audioSampleRateEdit;
    /// 音频格式
    QLabel* m_audioFormatLabel;
    QLineEdit* m_audioFormatEdit;
    /// 声道数
    QLabel* m_audioChannelLabel;
    QLineEdit* m_audioChannelEdit;




};

#endif // INFOWINDOW_H
