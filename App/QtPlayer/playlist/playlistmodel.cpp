﻿#include "playlistmodel.h"

#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QIODeviceBase>
#include <QImage>
#include <QUrl>

#include "fmp_frame_grabber.h"

PlayListModel::PlayListModel(QObject* parent) {
	m_playlist = new QList<Playitem>();
	m_currentIndex = -1;
	m_currentPlayitem = Playitem();
}

int PlayListModel::CurrentIndex() { return m_currentIndex; }

bool PlayListModel::SetCurrentIndex(int newCurrentIndex) {
	//合法性判断
	if ((newCurrentIndex < 0 && newCurrentIndex != -1) ||
		newCurrentIndex >= m_playlist->size()) {
		qDebug() << "PlayListModel::SetCurrentIndex:" << newCurrentIndex
			<< "非法, modelSize=" << m_playlist->size();
		return false;
	}
	//赋值
	emit CurrentIndexChanged(m_currentIndex, newCurrentIndex);
	m_currentIndex = newCurrentIndex;
	m_currentPlayitem =
		m_currentIndex == -1 ? Playitem() : m_playlist->at(newCurrentIndex);
	qDebug() << "newCurrentIndex: " << newCurrentIndex;

	return true;
}

const Playitem& PlayListModel::currentPlayitem() const {
	return m_currentPlayitem;
}

void PlayListModel::AddMedia(const QString& path, int pos) {
	auto grabber = FmpFrameGrabber::Instance();


	QFileInfo file = QFileInfo(path);
	QString fileName = file.fileName();  //获取文件名
	QString imageName = fileName.split(".").at(0) + ".png";
	QFileInfo info(imageName);
	//防止视频名相同时覆盖相同封面
	int i = 1;
	while (info.exists()) {
		imageName =
			fileName.split(".").at(0) + QString::number(i++) + ".png";
		info = QFileInfo(imageName);  //防止拖入多个同名视频导致覆盖的问题
	}
	//获取封面
	auto pathTmp = QUrl(path).toLocalFile();
	QImage cover = grabber->GetCover(pathTmp, imageName);
	if (cover.isNull()) {
		assert(0);
		//return;
	}
	auto coverResize = cover.scaled(QSize(200, 200));
	bool result = coverResize.save(imageName, "PNG");

	Playitem item;
	item.path = pathTmp;
	item.fileName = file.fileName();
	item.iconPath = imageName;
	//插入playitem
	int insertPos =
		pos == -1 ? this->m_playlist->size() : pos;  //取插入位置，
	this->m_playlist->insert(insertPos, item);  //将playitem保存到列表中
	emit PlayItemInserted(insertPos, insertPos);

	//                //检测当前是否没有在播放,如果是，则播放当前插入的视频
	//                if (CurrentIndex() == -1) {
	//                    SetCurrentIndex(0);
	//                }

		//新加item，刷新缓存
	Save();
	qDebug() << "curindex:" << m_currentIndex;
}

void PlayListModel::AddMedia() {
	for (int i = 0; i < m_playlist->size(); ++i) {
		Playitem item = m_playlist->at(i);
		QFileInfo source(item.path);
		if (!source.exists()) {
			qDebug() << item.path << "文件移动";
			// continue;
		}
		emit PlayItemInserted(i, i);
	}
}

void PlayListModel::RemoveMedias(QList<int> indexs) {
	int reback = 0;
	bool deleteCurFlag = false;  //判断是否删除当前播放的视频
	for (auto iter = indexs.constEnd() - 1; iter >= indexs.constBegin(); iter--) {
		int index = *iter;
		qDebug() << "remove index:" << index;
		//删除model内容
		m_playlist->remove(index);
		//通知view-model删除
		emit PlayItemRemoved(index);
		//        if (this->currentIndex() == index) {//把正在播放的内容删了
		//            deleteCurFlag = true;
		//            this->setCurrentIndex(-1);//播放器清空
		//        }
		if (index < m_currentIndex) {
			reback++;
		}
	}
	if (!deleteCurFlag) {
		int newCurrentIndex = this->CurrentIndex() - reback;
		SetCurrentIndex(newCurrentIndex);
	}
	Save();
}

const Playitem& PlayListModel::PlayitemAt(int i) {
	if (i < 0 || i >= m_playlist->size()) {
		qDebug() << "playitemAt " << i << " failed";
		assert(0);
		std::exit(-1);
	}
	return m_playlist->at(i);
}

int PlayListModel::Size() { return m_playlist ? m_playlist->size() : 0; }

void PlayListModel::Load() {
	QFile recordFile("playlist_cache");
	if (recordFile.open(QIODeviceBase::ReadOnly)) {
		qDebug() << "load";
		QDataStream stream(&recordFile);
		m_playlist->clear();
		stream >> *m_playlist;
		recordFile.close();
		for (const auto& t : *m_playlist) {
			qDebug() << t.fileName << ' ' << t.iconPath << " " << t.path;
		}
		AddMedia();
	}
}

void PlayListModel::Save() {
	QFile recordFile("playlist_cache");
	if (recordFile.open(QIODeviceBase::WriteOnly)) {
		qDebug() << "save";
		QDataStream stream(&recordFile);
		stream << *m_playlist;
		recordFile.close();
	}
}
