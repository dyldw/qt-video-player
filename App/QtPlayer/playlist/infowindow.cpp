﻿#include "infowindow.h"

#include <map>
#include <string>

#include "status_mgr.h"
#include "AVThread/AVThread.h"



InfoWindow::InfoWindow(QWidget *parent)
    :QMainWindow{parent}
{
    this->setCentralWidget(new QWidget());
    this->resize(QSize(540,320));

    m_rowLayout = new QVBoxLayout(this);
    m_filePathLabel = new QLabel(this);
    m_filePathLabel->setText("文件路径");
    m_filePathEdit = new QLineEdit(this);
    m_filePathEdit->setReadOnly(true);

    m_sizeLabel = new QLabel(this);
    m_sizeLabel->setText("视频宽高");
    m_sizeEdit = new QLineEdit(this);
    m_sizeEdit->setReadOnly(true);

    m_frameRateLabel = new QLabel(this);
    m_frameRateLabel->setText("视频帧率");
    m_frameRateEdit = new QLineEdit(this);
    m_frameRateEdit->setReadOnly(true);

    m_bitRateLabel = new QLabel(this);
    m_bitRateLabel->setText("视频码率");
    m_bitRateEdit = new QLineEdit(this);
    m_bitRateEdit->setReadOnly(true);

    m_videoFormatLabel = new QLabel(this);
    m_videoFormatLabel->setText("视频格式");
    m_videoFormatEdit = new QLineEdit(this);
    m_videoFormatEdit->setReadOnly(true);

    m_audioSampleRateLabel = new QLabel(this);
    m_audioSampleRateLabel->setText("音频采样率");
    m_audioSampleRateEdit = new QLineEdit(this);
    m_audioSampleRateEdit->setReadOnly(true);

    m_audioFormatLabel = new QLabel(this);
    m_audioFormatLabel->setText("音频格式");
    m_audioFormatEdit = new QLineEdit(this);
    m_audioFormatEdit->setReadOnly(true);

    m_audioChannelLabel = new QLabel(this);
    m_audioChannelLabel->setText("声道数");
    m_audioChannelEdit = new QLineEdit(this);
    m_audioChannelEdit->setReadOnly(true);

    //this->centralWidget()->setLayout(m_rowLayout);

    SetupLayout();

}

void InfoWindow::SetupLayout()
{
    QVBoxLayout *filePathLayout = new QVBoxLayout(this);
    filePathLayout->addWidget(m_filePathLabel,1);
    //filePathLayout->addSpacing(10);
    filePathLayout->addWidget(m_filePathEdit,4);
    m_rowLayout->addLayout(filePathLayout,1);

    QHBoxLayout *sizeLayout = new QHBoxLayout(this);
    filePathLayout->addWidget(m_sizeLabel,1);
    //filePathLayout->addSpacing(10);
    filePathLayout->addWidget(m_sizeEdit,4);
    m_rowLayout->addLayout(sizeLayout,1);

    QHBoxLayout *frameRateLayout = new QHBoxLayout(this);
    filePathLayout->addWidget(m_frameRateLabel,1);
    //filePathLayout->addSpacing(10);
    filePathLayout->addWidget(m_frameRateEdit,4);
    m_rowLayout->addLayout(frameRateLayout,1);

    QHBoxLayout *bitRateLayout = new QHBoxLayout(this);
    filePathLayout->addWidget(m_bitRateLabel,1);
    //filePathLayout->addSpacing(10);
    filePathLayout->addWidget(m_bitRateEdit,4);
    m_rowLayout->addLayout(bitRateLayout,1);

    QHBoxLayout *videoFormatLayout = new QHBoxLayout(this);
    filePathLayout->addWidget(m_videoFormatLabel,1);
    //filePathLayout->addSpacing(10);
    filePathLayout->addWidget(m_videoFormatEdit,4);
    m_rowLayout->addLayout(videoFormatLayout,1);

    QHBoxLayout *audioBitRateLayout = new QHBoxLayout(this);
    filePathLayout->addWidget(m_audioSampleRateLabel,1);
    //filePathLayout->addSpacing(10);
    filePathLayout->addWidget(m_audioSampleRateEdit,4);
    m_rowLayout->addLayout(audioBitRateLayout,1);

    QHBoxLayout *audioFormatLayout = new QHBoxLayout(this);
    filePathLayout->addWidget(m_audioFormatLabel,1);
    //filePathLayout->addSpacing(10);
    filePathLayout->addWidget(m_audioFormatEdit,4);
    m_rowLayout->addLayout(audioFormatLayout,1);

    QHBoxLayout *audioChannelLayout = new QHBoxLayout(this);
    filePathLayout->addWidget(m_audioChannelLabel,1);
    //filePathLayout->addSpacing(10);
    filePathLayout->addWidget(m_audioChannelEdit,4);
    m_rowLayout->addLayout(audioChannelLayout,1);

    this->centralWidget()->setLayout(m_rowLayout);

    m_rowLayout->setSpacing(10);
}

void InfoWindow::UpdateInfo()
{
    const auto& Info = GetStatusMgr()->VideoInfo();
    auto t = Info.find("filename");
    if(t == Info.end())
        return;
    m_filePathEdit->setText(QString::fromStdString(t->second));
    t = Info.find("width");
    auto width = t->second;
    t = Info.find("height");
    auto height = t->second;
    m_sizeEdit->setText(QString::fromStdString(width + "*" + height));
    t = Info.find("frame_rate");
    m_frameRateEdit->setText(QString::fromStdString(t->second));
    t = Info.find("bit rate");
    m_bitRateEdit->setText(QString::fromStdString(t->second));
    t = Info.find("video_format");
    m_videoFormatEdit->setText(QString::fromStdString(t->second));
    t = Info.find("audio format");
    m_audioSampleRateEdit->setText(QString::fromStdString(t->second));
    t = Info.find("sample rate");
    m_audioFormatEdit->setText(QString::fromStdString(t->second));
    t = Info.find("channel nums");
    m_audioChannelEdit->setText(QString::fromStdString(t->second));
}


