﻿#include "playlistchild.h"

#include <QMimeData>
#include <QSizePolicy>
#include <QDebug>
#include <QIcon>
#include <QMenu>
#include <QFile>
#include <QStandardPaths>
#include <QFileDialog>
#include <QMessageBox>
#include <random>

PlayListChild::PlayListChild(QWidget* parent) :QMdiSubWindow(parent)
{
	this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);//设置无边框
	m_model = new PlayListModel(this);
	m_view = new QListView(this);
	m_view->setResizeMode(QListView::Adjust);
    m_infoWindow = new InfoWindow(this);
    m_infoWindow->hide();


	setAcceptDrops(true);//接受拖拽
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(m_view, &QWidget::customContextMenuRequested, this, &PlayListChild::OnMenuRequest);

	SetupViewModel();

	srand(time(0));
}

void PlayListChild::OnSizeChanged()
{
	m_view->resize(this->size());
	emit SizeChanged();
}

void PlayListChild::resizeEvent(QResizeEvent* resizeEvent)
{
	QMdiSubWindow::resizeEvent(resizeEvent);
	m_view->resize(this->size());
	emit SizeChanged();
}


void PlayListChild::SetupViewModel()
{
	m_viewModel = new QStandardItemModel(this);

	//view设置
	m_view->setModel(m_viewModel);
	m_view->setIconSize(QSize(50, 50));
	m_view->setFont(QFont("Times", 12));
	m_view->setDragEnabled(false);//不支持item拖动
	m_view->setSelectionMode(QAbstractItemView::ExtendedSelection);//开启多选模式
	m_view->setEditTriggers(QAbstractItemView::NoEditTriggers);//不可编辑
	m_view->setContextMenuPolicy(Qt::CustomContextMenu);

	SetupSigSlot();
	m_model->Load();

}

void PlayListChild::dragEnterEvent(QDragEnterEvent* event)
{
	event->accept();
}

void PlayListChild::dropEvent(QDropEvent* event) {
	auto urls = event->mimeData()->urls();//urls->Qlist<Qurl>
	QList<QString> fileNames;
	foreach(auto url, urls) {
		m_model->AddMedia(url.toString(), -1);
		//fileNames<<url.toString();
	}
	//m_playlist->addMedia(urls.toSting(),-1);
	qDebug() << urls;
	event->accept();
}

void PlayListChild::SetupSigSlot()
{
	//增加视频
	connect(m_model, &PlayListModel::PlayItemInserted, this, &PlayListChild::OnPlayItemInserted);
	//删除视频
	connect(m_model, &PlayListModel::PlayItemRemoved, this, &PlayListChild::OnPlayItemRemoved);
	//双击PlayList
	connect(m_view, &QListView::doubleClicked, this, &PlayListChild::OnPlaylistViewDoubleClicked);
	//改变选中高亮
	connect(m_model, &PlayListModel::CurrentIndexChanged, this, [this](int lastIndex, int curIndex) {
		//取消所有高亮
		for (int i = 0; i < m_viewModel->rowCount(); ++i) {
			auto item = m_viewModel->item(i);
			item->setBackground(QColor(255, 255, 255));
			qDebug() << "rowCount " << i;
		}
		//设置选中高亮
		if (curIndex == -1) return;
		auto curItem = m_viewModel->item(curIndex);
		curItem->setBackground(QGradient(QGradient::NewYork));
		});

}
void PlayListChild::OnPlayItemInserted(int start, int end)
{
	for (int i = start; i <= end; ++i) {
		//更新viewModel
		const Playitem& playitem = m_model->PlayitemAt(i);
		QIcon icon = QIcon(playitem.iconPath);
		QIcon testIcon = QIcon("C:/Users/windrisess/Desktop/202203121452/202203121452-封面.jpg");
		assert(!icon.isNull());
		QStandardItem* item = new QStandardItem(icon, playitem.fileName);
		m_viewModel->insertRow(i, item);
	}
}

void PlayListChild::OnPlayItemRemoved(int index)
{
	m_viewModel->removeRow(index);
}

void PlayListChild::OnMenuRequest(const QPoint& pos)
{
	qDebug() << pos;
	QMenu* menu = new QMenu();
	QAction* addAction = menu->addAction("添加");
	QAction* delAction = menu->addAction("删除");
    QAction* infoAction = menu->addAction("视频信息");
	connect(addAction, &QAction::triggered, this, &PlayListChild::OnAddPlayItem);
	connect(delAction, &QAction::triggered, this, &PlayListChild::OnDeletePlayItem);
    connect(infoAction, &QAction::triggered, this, &PlayListChild::OnOpenVideoInfo);
	menu->exec(QCursor::pos());
	delete menu;
}

void PlayListChild::OnAddPlayItem()
{
	const QStringList& files = QFileDialog::getOpenFileNames(this,
		tr("Choose Video"),//对话框title
		QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).value(0, QDir::homePath()),//设置对话框打开的默认路径
		tr("Video Files(*.mp4 *.mov)"));
	if (!files.isEmpty()) {//插入urls
		qDebug() << files;
		for (const auto& url : files) {
			m_model->AddMedia(url, -1);
		}
	}
	qDebug() << "add clicked";
}

void PlayListChild::OnDeletePlayItem()
{
	qDebug() << "delete clicked";
	//判断选中列表是否为空
	if (m_view->selectionModel()->selectedIndexes().isEmpty() == false) {//生成删除列表
		QModelIndexList selectedIndexes(m_view->selectionModel()->selectedIndexes());
		QList<int> deleteRows;
		for (QModelIndexList::const_iterator it = selectedIndexes.constEnd() - 1;
			it >= selectedIndexes.constBegin(); --it) {
			deleteRows.append((*it).row());
		}
		//删除model中的数据
		std::sort(deleteRows.begin(), deleteRows.end());
		m_model->RemoveMedias(deleteRows);
		//int index=m_playlist->currentIndex();
		//m_playlistView->setCurrentIndex(m_playlistModel->index(index,0));
	}
}

void PlayListChild::OnPlaylistViewDoubleClicked(const QModelIndex& index)
{
	m_model->SetCurrentIndex(index.row());
	qDebug() << m_model->currentPlayitem().path;
    QFileInfo file(m_model->currentPlayitem().path);
    if(!file.exists()){
        QMessageBox::warning(this,"视频丢失", m_model->currentPlayitem().path + "视频已丢失");
        return;
    }
	GetStatusMgr()->SendOpen(m_model->currentPlayitem().path);
}

void PlayListChild::Previous()
{
	if (GetStatusMgr()->playMode() == PlayMode::kSequence) {
		m_model->SetCurrentIndex(((m_model->CurrentIndex() - 1) + m_model->Size()) % m_model->Size());
		qDebug() << m_model->currentPlayitem().path;
	}
	else if (GetStatusMgr()->playMode() == PlayMode::kRandom) {
		m_model->SetCurrentIndex(rand() % m_model->Size());
		qDebug() << m_model->currentPlayitem().path;
	}
	else if (GetStatusMgr()->playMode() == PlayMode::kSingle) {
		qDebug() << m_model->currentPlayitem().path;
	}
	GetStatusMgr()->SendOpen(m_model->currentPlayitem().path);

}

void PlayListChild::Next()
{
	if (GetStatusMgr()->playMode() == PlayMode::kSequence) {
		m_model->SetCurrentIndex((m_model->CurrentIndex() + 1) % m_model->Size());
		qDebug() << m_model->currentPlayitem().path;
	}
	else if (GetStatusMgr()->playMode() == PlayMode::kRandom) {
		m_model->SetCurrentIndex(rand() % m_model->Size());
		qDebug() << m_model->currentPlayitem().path;
	}
	else if (GetStatusMgr()->playMode() == PlayMode::kSingle) {
		qDebug() << m_model->currentPlayitem().path;
	}
	GetStatusMgr()->SendOpen(m_model->currentPlayitem().path);

}

void PlayListChild::OnOpenVideoInfo()
{
	m_infoWindow->UpdateInfo();
    m_infoWindow->show();
}
