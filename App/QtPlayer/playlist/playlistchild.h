﻿#ifndef PLAYLISTCHILD_H
#define PLAYLISTCHILD_H

#include <QObject>
#include <QMdiSubWindow>
#include <QListView>
#include <QStandardItemModel>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QListView>

#include "playlistmodel.h"
#include "status_mgr.h"
#include "infowindow.h"


class PlayListChild : public QMdiSubWindow
{
	Q_OBJECT
public:
	explicit PlayListChild(QWidget* parent = nullptr);
	void OnSizeChanged();
	void resizeEvent(QResizeEvent* resizeEvent) override;
private:
	void SetupViewModel();

	/**
	 * @brief Qt定义drag事件
	 * @param event
	 */
	void dragEnterEvent(QDragEnterEvent* event) override;

	/**
	 * @brief Qt定义drop事件
	 * @param event
	 */
	void dropEvent(QDropEvent* event) override;

	/**
	 * @brief 注册槽函数
	 */
	void SetupSigSlot();
signals:
	/**
	 * @brief 发送窗口大小改变信号
	 */
	void SizeChanged();

public slots:

	/**
	 * @brief 响应model变化，增加view-model中的数据
	 * @note [start,end)
	 * @param start model变化的开始序号
	 * @param end model变化的结束序号
	 */
	void OnPlayItemInserted(int start, int end);

	/**
	 * @brief 响应model变化，删除view-model中的数据
	 * @param index
	 */
	void OnPlayItemRemoved(int index);

	/**
	 * @brief 创建右键菜单
	 * @param pos 鼠标位置
	 */
	void OnMenuRequest(const QPoint& pos);

	/**
	 * @brief 右键导入音视频
	 */
	void OnAddPlayItem();

	/**
	 * @brief 右键删除选中的视频
	 */
	void OnDeletePlayItem();

	/**
	 * @brief 双击列表
	 * @param index
	 */
	void OnPlaylistViewDoubleClicked(const QModelIndex& index);

	/**
	 * @brief 上一视频
	 */
	void Previous();

	/**
	 * @brief 下一视频
	 */
	void Next();

    /**
     * @brief 打开视频信息
     */
    void OnOpenVideoInfo();

private:
	///MVVM中的model
	PlayListModel* m_model;

	///MVVM中的view
	QListView* m_view;

	///MVVM中的view-model
	QStandardItemModel* m_viewModel;

    ///视频信息窗口
    InfoWindow* m_infoWindow;

};

#endif // PLAYLISTCHILD_H
