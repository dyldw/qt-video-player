﻿#ifndef PLAYLISTMODEL_H
#define PLAYLISTMODEL_H

#include <QObject>

struct Playitem {
	QString path; //视频文件完整路径
	QString fileName; //视频文件名
	QString iconPath; //视频封面
	Playitem() {};
	friend QDataStream& operator<<(QDataStream& datastream, const Playitem& playitem) {
		datastream << playitem.path << playitem.fileName << playitem.iconPath;
		return datastream;
	}
	friend QDataStream& operator>>(QDataStream& datastream, Playitem& playitem) {
		datastream >> playitem.path >> playitem.fileName >> playitem.iconPath;
		return datastream;
	}
};


class PlayListModel :public QObject
{
	Q_OBJECT
		Q_PROPERTY(int CurrentIndex READ CurrentIndex WRITE SetCurrentIndex NOTIFY CurrentIndexChanged)
		Q_PROPERTY(Playitem currentPlayitem READ currentPlayitem NOTIFY CurrentPlayitemChanged)

public:
	PlayListModel(QObject* parent = nullptr);

	int CurrentIndex();
	bool SetCurrentIndex(int newCurrentIndex);
	const Playitem& currentPlayitem() const;
	void AddMedia(const QString& path, int pos = -1);
	void AddMedia();
	void RemoveMedias(QList<int> indexs);
	const Playitem& PlayitemAt(int i);

	/**
	 * @brief 获取model的item数
	 * @return model的item数
	 */
	int Size();

	void Load();
	void Save();

signals:
	void CurrentIndexChanged(int lastIndex, int curIndex);
	void CurrentPlayitemChanged();
	void PlayItemInserted(int start, int end);
	void PlayItemRemoved(int index);

private:
	QList<Playitem>* m_playlist;
	int m_currentIndex = -1;
	Playitem m_currentPlayitem;
};

#endif // PLAYLISTMODEL_H
