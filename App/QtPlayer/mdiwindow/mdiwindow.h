﻿#ifndef _MDI_WINDOW_H
#define _MDI_WINDOW_H

#include <qmainwindow.h>
#include <qmdiarea.h>
#include <qmdisubwindow.h>

#include "sdlchild.h"
#include "playlistchild.h"
#include "controlbar.h"
#include "previewchild.h"

class MdiWindow : public QMainWindow {
	Q_OBJECT
public:
	MdiWindow();
	void resizeEvent(QResizeEvent* resizeEvent) override;
	void keyPressEvent(QKeyEvent* event) override;
	void keyReleaseEvent(QKeyEvent* event) override;
    void mouseMoveEvent(QMouseEvent* e) override;
	void InitSDLWindow();
private:
	void SetupSigSlot();
public slots:
	void UpdateLayout();
	void OnFullScreen();
	void OnExitFullScreen();
	void OnOpen();
	void OnPlay();
	void OnStop();
	void OnPrev();
	void OnNext();
private:
	/// 主窗口
	QMdiArea* m_mdiArea;
	/// sdl窗口
	SDLChild* m_sdlChild;
	/// 媒体库窗口
	PlayListChild* m_playListChild;
	/// 控制块窗口
	ControlBar* m_controlBar;
	/// 预览窗口
	PreviewChild* m_previewChild;
	/// 唤醒控制条的计时器
	QTimer* m_raiseTimer;
};

#endif
