﻿#include "mdiwindow.h"

#include <QBoxLayout>
#include <QTimer>
#include "mywidget.h"

MdiWindow::MdiWindow() : m_mdiArea(new QMdiArea) {

	qDebug() << "MdiWindow init!";
	m_mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded); // Show scroll bars as necessary
	m_mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	this->setMouseTracking(true);
	setCentralWidget(m_mdiArea); // QMainWindow basics
	setWindowTitle("QMainWindow with MdiArea SDL Rendering Example");
	setBaseSize(640, 480);

	m_playListChild = new PlayListChild(this);
	m_controlBar = new ControlBar(this);
	m_previewChild = new PreviewChild(this);

	m_sdlChild = new SDLChild(this);	
	m_sdlChild->setWindowTitle("MDI SDL Child");

	m_mdiArea->addSubWindow(m_sdlChild);
	m_mdiArea->addSubWindow(m_playListChild);
	m_mdiArea->addSubWindow(m_controlBar);

	m_raiseTimer = new QTimer(this);
	m_raiseTimer->setSingleShot(true);

	UpdateLayout();
	SetupSigSlot();
    setFocusPolicy(Qt::StrongFocus);
	installEventFilter(this);
}

void MdiWindow::UpdateLayout()
{
	qDebug() << "mdiWindow update layout!";
	if (nullptr == m_sdlChild || nullptr == m_playListChild || nullptr == m_controlBar) {
		return;
	}
	m_sdlChild->setGeometry(0, 0, width() * 0.8, height() * 0.9);
	//m_sdlChild->setFixedSize(,height());
	//m_sdlChild->setFixedSize(m_sdlChild->size());
	m_sdlChild->UpdateLayout();
	m_playListChild->setGeometry(m_sdlChild->width(), 0, width() * 0.2, height());
	m_controlBar->setGeometry(0, height() * 0.9, width() * 0.8, height() * 0.1);
	m_controlBar->UpdateLayout();
}

void MdiWindow::OnFullScreen()
{
	m_playListChild->hide();
	this->showFullScreen();
	m_sdlChild->setGeometry(0, 0, width(), height());
	m_controlBar->setGeometry(0.1* width(), 0.9 * height(),0.8 * width(), 0.1 * height());
	m_controlBar->raise();
	m_raiseTimer->start(2000);
}

void MdiWindow::OnExitFullScreen()
{
	m_raiseTimer->stop();
	this->showNormal();
	m_playListChild->show();
	m_controlBar->show();
	UpdateLayout();
}

void MdiWindow::OnOpen()
{

}

void MdiWindow::OnPlay()
{
	m_sdlChild->SDLPlay();
}

void MdiWindow::OnStop()
{
	m_sdlChild->SDLPause();
}

void MdiWindow::OnPrev()
{

}

void MdiWindow::OnNext()
{

}

void MdiWindow::resizeEvent(QResizeEvent* resizeEvent)
{
	UpdateLayout();
}

void MdiWindow::keyPressEvent(QKeyEvent* event)
{
    if((event ->modifiers() & Qt::ControlModifier) != 0){
		switch (event->key()) {
		case Qt::Key_Escape:
			OnExitFullScreen();
			break;
		case Qt::Key_F:
			if (GetStatusMgr()->IsFullScreen()) {
				GetStatusMgr()->SetIsFullScreen(false);
				OnExitFullScreen();
			}
			else {
				GetStatusMgr()->SetIsFullScreen(true);
				OnFullScreen();
			}
			break;
		}
    }
	QMainWindow::keyPressEvent(event);
}

void MdiWindow::keyReleaseEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Space) {
		emit m_controlBar->GetPlayBtn()->clicked();
	}
	if ((event->modifiers() & Qt::ControlModifier) != 0) {
		switch (event->key()) {
		case Qt::Key_Left:
			emit m_controlBar->GetPrevBtn()->clicked();
			break;

		case Qt::Key_Right:
			emit m_controlBar->GetNextBtn()->clicked();
			break;
		case Qt::Key_Up:
			GetStatusMgr()->SetVolume(GetStatusMgr()->Volume() + 10);
			break;
		case Qt::Key_Down:
			GetStatusMgr()->SetVolume(GetStatusMgr()->Volume() - 10);
			break;
		}
	}
    QMainWindow::keyReleaseEvent(event);
}

void MdiWindow::mouseMoveEvent(QMouseEvent *e)
{
	if (GetStatusMgr()->IsFullScreen()) {
		m_raiseTimer->stop();
		m_raiseTimer->start(3000);
		m_controlBar->show();
		m_controlBar->raise();
	}
	QMainWindow::mouseMoveEvent(e);
}

void MdiWindow::InitSDLWindow()
{
	m_sdlChild->SDLInit();
}

void MdiWindow::SetupSigSlot()
{
	//预览
	connect(m_controlBar->GetProgressSlider(), &MySlider::Preview, m_previewChild, &PreviewChild::OnPreview);

	//改变Size
	connect(m_playListChild, &PlayListChild::SizeChanged, this, &MdiWindow::UpdateLayout);
	//结束预览
	connect(m_controlBar->GetProgressSlider(), &MySlider::PreviewEnd, m_previewChild, &PreviewChild::OnPreviewEnd);
	//全屏    退出全屏用全局快捷键ESC
	connect(m_sdlChild, &SDLChild::FullScreen, this, &MdiWindow::OnFullScreen);

	connect(m_controlBar->GetNextBtn(), &QPushButton::clicked, m_playListChild, &PlayListChild::Next);
	connect(m_controlBar->GetPrevBtn(), &QPushButton::clicked, m_playListChild, &PlayListChild::Previous);

	//点击进度条seek
	connect(m_controlBar->GetProgressSlider(), &MySlider::Click, m_sdlChild, &SDLChild::SDLSeek);

	connect(m_raiseTimer, &QTimer::timeout, this, [this]() {
		m_controlBar->hide();
		});
	// 鼠标移动
	connect(dynamic_cast<MyWidget*>(m_sdlChild->widget()), &MyWidget::mouseMove, this, &MdiWindow::mouseMoveEvent);
}


