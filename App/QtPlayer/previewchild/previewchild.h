﻿#ifndef PREVIEWCHILD_H
#define PREVIEWCHILD_H

#include<QMdiSubWindow>
#include<QLabel>
#include<mutex>

class PreviewChild :public QMdiSubWindow
{
	Q_OBJECT
public:
	PreviewChild(QWidget* parent = nullptr);
public slots:
	void OnPreview(int time);
	void OnPreviewEnd();
	void GetPreviewImage();

private:
	void SetupSigSlot();

private:
	QLabel* m_label;
	QTimer* m_previewTimer;
	int m_winWidth;
	int m_winHeight;

	int m_previewTime;//预览的时间
};

#endif // PREVIEWCHILD_H
