﻿#include "previewchild.h"

#include <QTimer>
#include <QString>

#include <QImage>
#include <QPixmap>
#include <QPainter>
#include <QPaintEvent>

#include "fmp_frame_grabber.h"
#include "status_mgr.h"

PreviewChild::PreviewChild(QWidget* parent)
{
	this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
	setWidget(new QWidget);
	m_winWidth = 200;
	m_winHeight = 200;
	m_label = new QLabel(this);
	m_previewTimer = new QTimer(this);
	m_previewTimer->setSingleShot(true);
	m_label->setGeometry(0, 0, m_winWidth, m_winHeight);

	this->moveToThread(FmpFrameGrabber::grabber_thread);//复用获取图像的线程

	connect(m_previewTimer, &QTimer::timeout, this, &PreviewChild::GetPreviewImage);
}

void PreviewChild::OnPreview(int time)//second
{
	qDebug() << m_previewTimer->isSingleShot();
	m_previewTimer->start(100);
	m_previewTime = time;
}


void PreviewChild::OnPreviewEnd()
{
	m_previewTimer->stop();
	qDebug("onPreviewEnd");
	this->hide();
}

void PreviewChild::GetPreviewImage()
{
	qDebug() << "GetPreviewImage()";

	//m_label->setText(QString::fromStdString(std::to_string(time)));

	QPoint p = QCursor::pos();
	this->setGeometry(p.x() - m_winWidth, p.y() / 20 * 20 - m_winHeight, m_winWidth, m_winHeight);

	auto img = FmpFrameGrabber::Instance()->GetFrame(QString::fromStdString(GetStatusMgr()->GetPath()), QString::number(m_previewTime), "./preview.png");
	if (img.isNull()) {
		assert(0);
	}
	m_label->setPixmap(QPixmap::fromImage(img));
	if (!isVisible()) {
		this->show();
	}
}




