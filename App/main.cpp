﻿#include <qapplication.h>
#include <SDL.h>
#include <QFile>
#include <QThread>

#include <gflags/gflags.h>

#include "mdiwindow.h"
#include "../Kernel/AVPlayer/AVPlayer.h"

int main(int argc, char* argv[]) {

//	std::system("chcp 65001");
	gflags::ParseCommandLineFlags(&argc, &argv, true);

	QApplication a(argc, argv); // Basics of QT, start an application

	MdiWindow mdiWindow; // Creating and using a QMainWindow with an Mdi Area
	mdiWindow.show();
	//mdiWindow.InitSDLWindow();

	int retVal = a.exec();	// Most examples have this on the return, we
							// need to have it return to a variable cause:

	return retVal;

}
