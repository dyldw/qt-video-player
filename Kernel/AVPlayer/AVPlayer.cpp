﻿#include "AVPlayer.h"
#include "Comm/MessageBus.hpp"
#include <intrin0.inl.h>
#include <stdint.h>
#include <stdio.h>

namespace AV {
MediaPlayer::MediaPlayer() {
  std::unique_lock<std::mutex> locker(mutex_);
  LOGIMPT("construct MediaPlayer begin");
  _Init();
  LOGIMPT("construct MediaPlayer end");
}

MediaPlayer::~MediaPlayer() {
  std::unique_lock<std::mutex> locker(mutex_);
  LOGIMPT("Begin deconstruct MediaPlayer");
  // oDemuxThread_.ExitLoop();
  // oAudioDecodeThread_.ExitLoop();
  // oVideoDecodeThread_.ExitLoop();
  // oVideoRenderThread_.ExitLoop();
  // oAudioRenderThread_.ExitLoop();
  LOGIMPT("deconstruct MediaPlayer");
}

void MediaPlayer::Open(std::string const& filename, void* windowHandle) {
  std::unique_lock<std::mutex> locker(mutex_);
  // 建立音视频同步对象
  pAVSynchronizer_ = std::make_shared<AudioVideoSynchronizer>();

  // 初始化线程
  LOGINFO("init mediaPlayer thread");
  int ret =
      oDemuxThread_.Init(filename, pVideoPacketQueue_, pAudioPacketQueue_);
  LOGERROR_IF(ret, "DemuxThread init error! with ret = %d", ret);
  oVideoDecodeThread_.SetName("VideoDecodeThread");

  // 判断是否存在视频
  auto videoCodecPar = oDemuxThread_.GetVideoCodecPar();
  onlyAudio_ = bool(videoCodecPar == nullptr);

  if (onlyAudio_ == false) {
    ret = oVideoDecodeThread_.Init(oDemuxThread_.GetVideoCodecPar(),
                                   pVideoPacketQueue_, pVideoFrameQueue_);
	oVideoDecodeThread_.SetFrameRate( oDemuxThread_.GetVideoFrameRate() );
	oVideoDecodeThread_.SetTimeBase( oDemuxThread_.GetVideoTimeBase() );
    oVideoDecodeThread_.SetName("VideoDecodeThread");
    LOGERROR_IF(ret, "VideoDecodeThread init error! with ret = %d", ret);
  }

  oAudioDecodeThread_.SetFrameRate( oDemuxThread_.GetVideoFrameRate() );
  oAudioDecodeThread_.SetTimeBase( oDemuxThread_.GetAudioTimeBase() );
  oAudioDecodeThread_.SetName("AudioDecodeThread");

  ret = oAudioDecodeThread_.Init(oDemuxThread_.GetAudioCodecPar(),
                                 pAudioPacketQueue_, pAudioFrameQueue_);
  LOGERROR_IF(ret, "AudioDecodeThread init error! with ret = %d", ret);

  if (onlyAudio_ == false) {
    ret = oVideoRenderThread_.Init(pVideoFrameQueue_, pAVSynchronizer_,
                                   windowHandle);
	oVideoRenderThread_.SetFrameRateDiv( oDemuxThread_.GetVideoFrameRateDiv() );
    LOGERROR_IF(ret, "VideoRenderThread init error! with ret = %d", ret);
  }

  ret = oAudioRenderThread_.Init(oDemuxThread_.GetAudioCodecPar(),
                                 pAudioFrameQueue_, pAVSynchronizer_);
  LOGERROR_IF(ret, "AudioRenderThread init error! with ret = %d", ret);
  oAudioRenderThread_.SetPCMQueue(pPCMQueue_);

  MessageBus::Attach("videorender_can_get_frame", &VideoRenderThread::toggle_can_pop, &oVideoRenderThread_);
  MessageBus::Attach("audiorender_can_get_frame", &AudioRenderThread::toggle_can_pop, &oAudioRenderThread_);
  MessageBus::Attach("demuxer_can_push", &DemuxThread::toggle_can_push, &oDemuxThread_);

  MessageBus::Attach("videodecoder_can_decode_Frame", &DecodeThread::toggle_EndofPacket,&oVideoDecodeThread_);
  MessageBus::Attach("videodecoder_notify_last_packet", &DecodeThread::notify_last_packet, &oVideoDecodeThread_);

  MessageBus::Attach("audiodecoder_can_decode_Frame", &DecodeThread::toggle_EndofPacket,&oAudioDecodeThread_);
  MessageBus::Attach("audiodecoder_notify_last_packet", &DecodeThread::notify_last_packet, &oAudioDecodeThread_);

  // 启动线程
  LOGINFO("Start mediaPlayer thread");
  oDemuxThread_.StartLoop();
  oAudioDecodeThread_.StartLoop();
  oVideoDecodeThread_.StartLoop();
  oVideoRenderThread_.StartLoop();
  oAudioRenderThread_.StartLoop();
}

void MediaPlayer::Play() {
  std::unique_lock<std::mutex> locker(mutex_);
  LOGINFO("Play action");
  oDemuxThread_.Resume();
  oAudioDecodeThread_.Resume();
  if (onlyAudio_ == false) {
    oVideoDecodeThread_.Resume();
  }
  // 等两秒让缓冲队列填满一点，这样播放更流畅
  // std::this_thread::sleep_for(std::chrono::seconds(2));
  if (onlyAudio_ == false) {
    oVideoRenderThread_.Resume();
  }
  oAudioRenderThread_.Resume();
}

void MediaPlayer::Pause() {
  std::unique_lock<std::mutex> locker(mutex_);
  LOGINFO("Pause action");
  LOGIMPT("Begin Suspend");
  // oDemuxThread_.Suspend();
  // oAudioDecodeThread_.Suspend();
  // oVideoDecodeThread_.Suspend();

  oAudioRenderThread_.Suspend();
  if (onlyAudio_ == false) {
    oVideoRenderThread_.Suspend();
  }
}

void MediaPlayer::Close() {
  std::unique_lock<std::mutex> locker(mutex_);
  LOGINFO("Close action");
  oDemuxThread_.ExitLoop();
  oAudioDecodeThread_.ExitLoop();
  oAudioRenderThread_.ExitLoop();

  if (onlyAudio_ == false) {
    oVideoDecodeThread_.ExitLoop();
    oVideoRenderThread_.ExitLoop();
  }
  MessageBus::Detach("videorender_can_get_frame");
  MessageBus::Detach("audiorender_can_get_frame");

  MessageBus::Detach("demuxer_can_push");

  MessageBus::Detach("videodecoder_can_decode_Frame");
  MessageBus::Detach("videodecoder_notify_last_packet");

  MessageBus::Detach("audiodecoder_can_decode_Frame");
  MessageBus::Detach("audiodecoder_notify_last_packet");
}

void MediaPlayer::Seek(double percent) {
  std::unique_lock<std::mutex> locker(mutex_);
  pAudioPacketQueue_->resize(360);
  pVideoPacketQueue_->resize(360);
  pAudioFrameQueue_->resize(360);
  pVideoFrameQueue_->resize(360);
  // 如何保证原子性？加锁？
  // 1. 暂停demux线程，音频解码线程，视频解码线程，音频渲染线程，视频渲染线程
  LOGIMPT("before seek : Suspend");
  ChildThreadSuspendFunc();

  ChildThreadClearFunc();

  // 4. 进行seek低精度seek。seek到最接近的上一个关键帧
  LOGIMPT("begin seek");
  // MessageBus::NotifyByTopic("Demuxer:seek", percent);
  int64_t seek_pts = oDemuxThread_.pDemuxer_->Seek(percent);
  LOGIMPT("end seek");
  // MessageBus::NotifyByTopic("Decoder:set_seek_pts", seek_pts); //
  // 阻塞通知decoder要seek到某个位置 5.1.
  // 给解码器设置seek模式，参数是seek期望的pts
  // 运行到对应pts会自动清除seekmode标志位
  oAudioDecodeThread_.pDecoder_->SetSeekMode(seek_pts);

  oVideoDecodeThread_.pDecoder_->SetSeekMode(seek_pts);

  // // 5.2. 刷新解码器上下文
  // oAudioDecodeThread_.pDecoder_->FlushBuffers(); // avcodec_flush_buffers
  // oVideoDecodeThread_.pDecoder_->FlushBuffers(); // avcodec_flush_buffers

  // 6. 恢复解复用线程，音频解码线程，视频解码线程 ()

  pAudioPacketQueue_->resize(50);
  pVideoPacketQueue_->resize(50);
  pAudioFrameQueue_->resize(50);
  pVideoFrameQueue_->resize(50);
  LOGIMPT("after seek : Resume");
  ChildThreadResumeFunc();

  LOGIMPT("Seek OK");
}

void MediaPlayer::SetSpeed(double speed_rate) {
  std::unique_lock<std::mutex> locker(mutex_);
  // 如何保证原子性？加锁？
  // 1. 暂停demux线程，音频解码线程，视频解码线程，音频渲染线程，视频渲染线程
  LOGIMPT("before seek : Suspend");

  ChildThreadSuspendFunc();

  LOGIMPT("before seek : Clear");
  ChildThreadClearFunc();

  oAudioRenderThread_.pAudio_resample_->SetSpeedRate(speed_rate);
  pAudioPacketQueue_->resize(20);
  pVideoPacketQueue_->resize(20);
  pAudioFrameQueue_->resize(20*speed_rate);
  pVideoFrameQueue_->resize(20*speed_rate);

  // 6. 恢复解复用线程，音频解码线程，视频解码线程 ()
  LOGIMPT("after seek : Resume");
  ChildThreadResumeFunc();
}

void MediaPlayer::SetVolume(double volume) {
  oAudioRenderThread_.pAudio_render_->SetVolume(volume);
}

void MediaPlayer::SetPCMQueue(std::shared_ptr<PCMQueue> pPCMQueue) {
  std::unique_lock<std::mutex> locker(mutex_);
  pPCMQueue_ = pPCMQueue;
}

void MediaPlayer::NextFrame() {
  std::unique_lock<std::mutex> locker(mutex_);
  // 如果没有暂停需要先暂停
  if (onlyAudio_ == false) {
    LOGIMPT("Call VideoRender suspend()");
    oVideoRenderThread_.Suspend();
  }

  LOGIMPT("Call AudioRender suspend()");
  oAudioRenderThread_.Suspend();

  if (onlyAudio_ == false) {
    oVideoDecodeThread_.RunOnce();
  }

  oAudioDecodeThread_.RunOnce();
}

void MediaPlayer::Reverse(){
  std::unique_lock<std::mutex> locker(mutex_);
  LOGIMPT("before Reverse: Suspend");
  ChildThreadSuspendFunc();
  LOGIMPT("before Reverse: Clear");
  ChildThreadClearFunc();

  reverse_ = !reverse_;

  tmp_pts_=reverse_?pAVSynchronizer_->GetSyncPts():0;

  ToggleReverse();

  LOGIMPT("after Reverse: Resume");
  ChildThreadResumeFunc();
}

void MediaPlayer::ToggleReverse() {
  if(reverse_)
  {
	  pAudioPacketQueue_->resize(360);
	  pVideoPacketQueue_->resize(360);
	  pAudioFrameQueue_->resize(360);
	  pVideoFrameQueue_->resize(360);
  }
  else
  {
	  pAudioPacketQueue_->resize(30);
	  pVideoPacketQueue_->resize(30);
	  pAudioFrameQueue_->resize(30);
	  pVideoFrameQueue_->resize(30);
  }
  oDemuxThread_.toggleReverse(tmp_pts_);
  oVideoRenderThread_.toggleReverse(tmp_pts_);
  oAudioRenderThread_.toggleReverse(tmp_pts_);
  oVideoDecodeThread_.toggleReverse(tmp_pts_);
  oAudioDecodeThread_.toggleReverse(tmp_pts_);
}

VideoInfo MediaPlayer::GetVideoInfo() const {
  std::unique_lock<std::mutex> locker(mutex_);
  return oDemuxThread_.pDemuxer_->GetVideoInfo();
}

bool MediaPlayer::CheckReverse(){
	return reverse_;
}

inline void MediaPlayer::ChildThreadSuspendFunc(){
  LOGIMPT("Call Demuxer suspend()");
  oDemuxThread_.Suspend();
  LOGIMPT("Call AudioDecode suspend()");
  oAudioDecodeThread_.Suspend();
  if (onlyAudio_ == false) {
    LOGIMPT("Call VideoDecode suspend()");
    oVideoDecodeThread_.Suspend();
    LOGIMPT("Call VideoRender suspend()");
    oVideoRenderThread_.Suspend();
  }
  LOGIMPT("Call AudioRender suspend()");
  oAudioRenderThread_.Suspend();

}

inline void MediaPlayer::ChildThreadClearFunc(){
  // 清空packet队列，frame队列
  LOGIMPT("clear queue buffer");
  pAudioPacketQueue_->clear();
  pVideoPacketQueue_->clear();
  pAudioFrameQueue_->clear();
  pVideoFrameQueue_->clear();

  // 清空解复用器读取缓冲，音频解码器缓存，视频解码器缓存，音频渲染缓存，视频渲染缓存
  LOGIMPT("clear thread buffer");
  oDemuxThread_.Clear();
  oAudioDecodeThread_.Clear();
  oAudioRenderThread_.Clear();
  if (onlyAudio_ == false) {
    oVideoDecodeThread_.Clear();
    oVideoRenderThread_.Clear();
  }
}

inline void MediaPlayer::ChildThreadResumeFunc(){
  LOGIMPT("Begin Resume");
  oDemuxThread_.Resume();
  oAudioDecodeThread_.Resume();
  oAudioRenderThread_.Resume();
  if (onlyAudio_ == false) {
    oVideoDecodeThread_.Resume();
    oVideoRenderThread_.Resume();
  }
}

void MediaPlayer::_Init() {
  // 建立队列
  LOGINFO("build queue");
  pAudioPacketQueue_ = std::make_shared<PacketQueue>(365);
  pVideoPacketQueue_ = std::make_shared<PacketQueue>(365);
  pAudioFrameQueue_ = std::make_shared<FrameQueue>(365);
  pVideoFrameQueue_ = std::make_shared<FrameQueue>(365);
	  pAudioPacketQueue_->resize(30);
	  pVideoPacketQueue_->resize(30);
	  pAudioFrameQueue_->resize(30);
	  pVideoFrameQueue_->resize(30);
  // 队列信息
  LOGONTEST("AudioPacketQueue.capacity = %u", pAudioPacketQueue_->capacity());
  LOGONTEST("VideoPacketQueue.capacity = %u", pVideoPacketQueue_->capacity());
  LOGONTEST("AudioFrameQueue.capacity = %u", pAudioFrameQueue_->capacity());
  LOGONTEST("VideoFrameQueue.capacity = %u", pVideoFrameQueue_->capacity());

}

}  // namespace AV
