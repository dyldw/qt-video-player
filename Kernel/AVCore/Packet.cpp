#include "AVCore.h"
#include <stdint.h>
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/packet.h>
}

namespace AV {

Packet::Packet() { pkt = av_packet_alloc(); }
Packet::~Packet() {
  if (pkt) {
    av_packet_free(&pkt);
    pkt = nullptr;
  }
}

void Packet::SetPts() { PtsInMilisecond=static_cast<int64_t>(GetOriginPts()*(1000*av_q2d(pkt->time_base))); }
int Packet::GetIndex() { return pkt->stream_index; }
int64_t Packet::GetPts() { return PtsInMilisecond; }
int64_t Packet::GetOriginPts() { return pkt->pts; }
bool Packet::isKeyPacket() { return pkt->flags & AV_PKT_FLAG_KEY; }
}  // namespace AV
