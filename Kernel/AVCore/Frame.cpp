#include <stdint.h>

#include <iostream>

#include "AVCore.h"
extern "C" {
#include <SDL.h>
#include <SDL_thread.h>
#include <libavformat/avformat.h>
#include <libavutil/pixdesc.h>
}

namespace AV {
Frame::Frame() { frame = av_frame_alloc(); }

Frame::~Frame() {
  if (frame != nullptr) {
    av_frame_free(&frame);
    frame = nullptr;
  }
}

void Frame::SetTimeBase(AVRational* time_base) {
//  if(frame->pict_type!=AV_PICTURE_TYPE_NONE)
//	if(frame->time_base.num==0 && frame->time_base.den==1 )
      frame->time_base = *time_base;
}

void Frame::SetPts() {
  if (!frame) return;
//  if (frame->time_base.num != 0)
      PtsInMilisecond = frame->pts * 1000 * av_q2d(frame->time_base);
//  else
//      PtsInMilisecond = frame->pts * 1000 * (1.0f / frame->sample_rate);
}

int64_t Frame::GetPts() {
  LOGERROR_IF(frame == nullptr, "frame is nullptr");
  if (!PtsInMilisecond) Frame::SetPts();
  return PtsInMilisecond;
}

int64_t Frame::GetOriginPts() {
  LOGERROR_IF(frame == nullptr, "frame is nullptr");
  return frame->pts;
}

int Frame::GetFormat() const {
  LOGERROR_IF(frame == nullptr, "frame is nullptr");
  return frame->format;
}
int Frame::VideoPrint() {
  // LOGINFO("width %d\theight %d", frame->width, frame->height);
  AVPixelFormat format = (AVPixelFormat)(frame->format);
  std::string str;
  str.resize(256);
  av_get_pix_fmt_string(&(str[0]), 256, format);
  LOGONTEST("Pixel Format: %s", str.c_str());

  // for (int i = 0; i < AV_NUM_DATA_POINTERS; i++) {
  //   LOGONTEST("Linesize[%d]: %d", i, frame->linesize[i]);
  // }
  return 0;
}

int Frame::AudioPrint() {
  // int channel = frame->channels;
  // LOGINFO("Channel: %d", channel);
  // LOGINFO("nb_samples: %d", frame->nb_samples);
  // LOGINFO("sample_rate: %d", frame->sample_rate);

  AVSampleFormat format = (AVSampleFormat)(frame->format);

  std::string str;
  str.resize(128);
  av_get_sample_fmt_string(&(str[0]), 128, format);
  LOGONTEST("Sample Format: %s", str.c_str());

  // for (int i = 0; i < AV_NUM_DATA_POINTERS; i++) {
  //   LOGONTEST("Linesize[%d]: %d", i, frame->linesize[i]);
  // }
  return 0;
}

}  // namespace AV
