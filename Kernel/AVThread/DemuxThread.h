#pragma once
#include "Common.h"
#include <stdint.h>

namespace AV {

class DemuxThread final : public ThreadEventLoop {
 public:
  DemuxThread();
  virtual ~DemuxThread();

  int Init(std::string const& filename,
           std::shared_ptr<PacketQueue> pVideo_packet_queue,
           std::shared_ptr<PacketQueue> pAudio_packet_queue);
  AVCodecParameters* GetVideoCodecPar();
  AVRational* GetVideoFrameRate();
  double GetVideoFrameRateDiv();
  AVRational* GetVideoTimeBase();
  AVRational* GetAudioTimeBase();
  AVCodecParameters* GetAudioCodecPar();
  int Seek(double percent);
  inline bool check_reverse_pos(int64_t pos,int PktIndex);
  void toggleReverse(int64_t);
  void toggle_can_push(int x);

 protected:
  virtual int RunOnce() override;
  friend MediaPlayer;

 private:
  virtual int ClearImpl() override;

 private:
  PacketPtr pPkt_;
  std::shared_ptr<Demuxer> pDemuxer_;
  std::shared_ptr<PacketQueue> pVideo_packet_queue_;
  std::shared_ptr<PacketQueue> pAudio_packet_queue_;
  uint32_t nVideo_index_;
  uint32_t nAudio_index_;

  bool reverse{false};
  bool can_push_Video{true};
  bool can_push_Audio{true};
  bool notified{ false };
  /* At the beginning of Reverse
   * reverse_seeked_pos(IFrame)  reverse_origin_pos             next_IFrame
   *           ↓                        ↓                        ↓
   * 21   22   23   24   25   26   27   28   29   30   31   32   33   34
   *
   * After Seek Once
   * reverse_seeked_pos                             reverse_origin_pos=next_IFrame
   *           ↓                                                 ↓
   * 11   12   13   14   15   16   17   18   19   20   21   22   23
   *
   * 所以对于解包和解码，应该要执行到下一关键帧（主要是解包），但是对于渲染器，应该是执行到reverse_origin_pos
   */
  int64_t reverse_origin_pos;
  int64_t reverse_seeked_pos;
  uint64_t reverse_seeked_origin_pts;
  int64_t next_IFrame_pos;
  int32_t be_notified_counter;
};
}  // namespace AV
