#pragma once
#include <condition_variable>

#include "Common.h"

namespace AV {

class AudioRenderThread final : public ThreadEventLoop {
 public:
  friend MediaPlayer;
  AudioRenderThread();
  ~AudioRenderThread();
  void toggle_can_pop(int x);
  int Init(AVCodecParameters* para,
           std::shared_ptr<FrameQueue> pInput_frame_queue,
           std::shared_ptr<AudioVideoSynchronizer> pAVSynchronizer);
  int SetPCMQueue(std::shared_ptr<PCMQueue> pPCMQueue);

 protected:
  virtual int RunOnce() override;

 private:
  virtual int SuspendImpl() override;
  virtual int ResumeImpl() override;
  virtual int ClearImpl() override;
  virtual int BeforeRun() override;
  void toggleReverse(int64_t pos);

 private:
  std::shared_ptr<FrameQueue> pInput_frame_queue_;
  std::shared_ptr<PCMQueue> pPCMQueue_;
  std::unique_ptr<Resample> pAudio_resample_;
  std::unique_ptr<AudioRenderBase> pAudio_render_;
  std::shared_ptr<AudioVideoSynchronizer> pAVSynchronizer_;

  bool reverse{false};
  int can_pop{0};
  /* At the beginning of Reverse
   * reverse_seeked_pos(IFrame)  reverse_origin_pos             next_IFrame
   *           ↓                        ↓                        ↓
   * 21   22   23   24   25   26   27   28   29   30   31   32   33   34
   *
   * After Seek Once
   * reverse_seeked_pos                             reverse_origin_pos=next_IFrame
   *           ↓                                                 ↓
   * 11   12   13   14   15   16   17   18   19   20   21   22   23
   *
   * 所以对于解包和解码，应该要执行到下一关键帧（主要是解包），但是对于渲染器，应该是执行到reverse_origin_pos
   */
  int64_t reverse_origin_pos{0};
};
}  // namespace AV
