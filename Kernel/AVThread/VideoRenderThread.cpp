﻿#include "VideoRenderThread.h"
#include <corecrt_math.h>
#include <stdint.h>

namespace AV {

VideoRenderThread::VideoRenderThread() {
  LOGIMPT("construct VideoRenderThread");
  SetName("VideoRenderThread");
}
VideoRenderThread::~VideoRenderThread() {
  ThreadEventLoop::BeforeDerivedClassDeconstruct();
  LOGIMPT("deconstruct VideoRenderThread");
}

int VideoRenderThread::Init(
    std::shared_ptr<FrameQueue> pInput_frame_queue,
    std::shared_ptr<AudioVideoSynchronizer> pAVSynchronizer,
    void* windowHandle) {
  LOGERROR_IF(!pInput_frame_queue, "pInput_frame_queue is nullptr");
  pInput_frame_queue_ = pInput_frame_queue;
  LOGERROR_IF(pAVSynchronizer == nullptr, "pAVSynchronizer is nullptr");
  pAVSynchronizer_ = pAVSynchronizer;
  if (pVideo_render_ == nullptr) {
    pVideo_render_ = std::make_unique<SDLVideoRender>();
    pVideo_render_->Init("mediaplayer test", 1920 , 1080 , windowHandle);
  }
  return 0;
}

int VideoRenderThread::RunOnce() {
  LOGONTEST("VideoRenderThread");

  if(reverse)
  {
    if(can_pop!=3) return 0;
	if(pInput_frame_queue_->size()==0)
	{
		MessageBus::Notify("demuxer_can_push", 0);
		can_pop=0;
		return 0;
	}
  }

  thread_local FramePtr pFrame=nullptr;
  LOGONTEST("Thread %s frame queue, queue size = %u, capacity = %u",
            GetName().c_str(), pInput_frame_queue_->size(),
            pInput_frame_queue_->capacity());
  if(reverse){
      if (pFrame->GetPts() > reverse_origin_pos) pInput_frame_queue_->dry_pop(
          ((pFrame->GetPts() - reverse_origin_pos) * Framerate / 1000) - 0, reverse
      );
  }
  if (pInput_frame_queue_->pop_timeout(pFrame, 20,reverse) == false) {
    LOGWARN("Thread %s frame queue, queue size = %u, capacity = %u",
            GetName().c_str(), pInput_frame_queue_->size(),
            pInput_frame_queue_->capacity());
    LOGWARN("Thread %s try to pop video frame queue timeout",
            GetName().c_str());
    return 0;
  }

  // 打印视频帧信息log
  if(pFrame==nullptr)
  {
    LOGIMPT("Thread %s no more frame to render", GetName().c_str());
    ThreadEventLoop::Finish();
	return 0;
  }
  LOGONTEST("print video frame info");
  pFrame->VideoPrint();


  // 音画同步
  LOGIMPT("Video Render Frame Pts: %lld",pFrame->GetPts());
  if(pAVSynchronizer_->ShouldDiscardVideoFrame(pFrame->GetPts(), reverse)) {
	LOGIMPT("Video Render Frame Pts: %lld",pFrame->GetPts());
    LOGONTEST("discard video frame");
    LOGTRACE("ShouldDiscardVideoFrame Video pts = %llu, sync pts = %llu",
             pFrame->GetPts(), pAVSynchronizer_->GetSyncPts());
    int frame_counter= (reverse ? (pFrame->GetPts() - pAVSynchronizer_->GetSyncPts()) : (pAVSynchronizer_->GetSyncPts() - pFrame->GetPts()))* Framerate * 1.0f / 1000;
    pInput_frame_queue_->dry_pop(frame_counter, reverse);
    if (pInput_frame_queue_->pop_timeout(pFrame, 20, reverse) == false) {
      LOGWARN("Thread %s frame queue, queue size = %u, capacity = %u",
              GetName().c_str(), pInput_frame_queue_->size(),
              pInput_frame_queue_->capacity());
      LOGWARN("Thread %s try to pop video frame queue timeout",
              GetName().c_str());
      return 0;
    }
  }
  uint32_t delay_duration =
      pAVSynchronizer_->VideoFrameDelayDuration(pFrame->GetPts(),reverse);
  if (delay_duration != 0) {
    LOGTRACE("Delay video frame render: %ums", delay_duration);
    //if (delay_duration > 100) return 0;
    std::this_thread::sleep_for(std::chrono::milliseconds(delay_duration));
  }
  // 渲染视频帧
  pVideo_render_->Render(pFrame);
  LOGTRACE("video frame pts = %llu", pFrame->GetPts());

  return 0;
}

int VideoRenderThread::ClearImpl() {
  pVideo_render_->Clear();  // 清空渲染器中的缓冲数据。
  if (pInput_frame_queue_ != nullptr)
  {
      pInput_frame_queue_->clear();
  }
  return 0;
}

void VideoRenderThread::SetFrameRateDiv(double framerate_div)
{
	Framerate = framerate_div;
}

void VideoRenderThread::toggleReverse(int64_t pos)
{
	reverse = !reverse;
	reverse_origin_pos=pos;
	can_pop = 0;
}

void VideoRenderThread::toggle_can_pop(int x){
  can_pop |= x;
}
}  // namespace AV
