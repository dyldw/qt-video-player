#pragma once
#include "Common.h"
#include <stdint.h>

namespace AV {

class VideoRenderThread final : public ThreadEventLoop {
 public:
  friend MediaPlayer;
  VideoRenderThread();
  ~VideoRenderThread();
  void toggle_can_pop(int x);
  void SetFrameRateDiv(double framerate_div);
  int Init(std::shared_ptr<FrameQueue> pInput_frame_queue,
           std::shared_ptr<AudioVideoSynchronizer> pAVSynchronizer,
           void* windowHandle);

 protected:
  virtual int RunOnce() override;

 private:
  virtual int ClearImpl() override;
  void toggleReverse(int64_t pos);

 private:
  std::shared_ptr<FrameQueue> pInput_frame_queue_;
  static inline std::unique_ptr<VideoRenderBase> pVideo_render_ = nullptr;
  std::shared_ptr<AudioVideoSynchronizer> pAVSynchronizer_;

  bool reverse{false};
  int can_pop{0};
  /* At the beginning of Reverse
   * reverse_seeked_pos(IFrame)  reverse_origin_pos             next_IFrame
   *           ↓                        ↓                        ↓
   * 21   22   23   24   25   26   27   28   29   30   31   32   33   34
   *
   * After Seek Once
   * reverse_seeked_pos                             reverse_origin_pos=next_IFrame
   *           ↓                                                 ↓
   * 11   12   13   14   15   16   17   18   19   20   21   22   23
   *
   * 所以对于解包和解码，应该要执行到下一关键帧（主要是解包），但是对于渲染器，应该是执行到reverse_origin_pos
  */
  int64_t reverse_origin_pos{0};
  double Framerate;
};
}  // namespace AV
