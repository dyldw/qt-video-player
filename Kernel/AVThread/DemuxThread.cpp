#include "DemuxThread.h"
#include <stdint.h>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
}

namespace AV {

DemuxThread::DemuxThread() {
  ThreadEventLoop::SetName("DemuxThread");
  pDemuxer_ = std::make_unique<Demuxer>();
  LOGIMPT("construct DemuxThread ");
}

DemuxThread::~DemuxThread() {
  ThreadEventLoop::BeforeDerivedClassDeconstruct();
  LOGIMPT("deconstruct DemuxThread ");
}

int DemuxThread::Init(std::string const& filename,
                      std::shared_ptr<PacketQueue> pVideo_packet_queue,
                      std::shared_ptr<PacketQueue> pAudio_packet_queue) {
  if (pVideo_packet_queue == nullptr || pAudio_packet_queue == nullptr) {
    LOGERROR("Thread %s Queue Ptr is nullptr", GetName().c_str());
  }
  int ret = pDemuxer_->Open(filename);
  LOGERROR_IF(ret, "Thread %s Open Error! filename = %s, ret = %d",
              GetName().c_str(), filename.c_str(), ret);
  nVideo_index_ = pDemuxer_->GetVideoStreamIndex();
  nAudio_index_ = pDemuxer_->GetAudioStreamIndex();
  LOGONTEST("Thread %s video stream index = %u, audio stream index = %u",
            GetName().c_str(), nVideo_index_, nAudio_index_);
  pVideo_packet_queue_ = pVideo_packet_queue;
  pAudio_packet_queue_ = pAudio_packet_queue;

  // std::weak_ptr<Demuxer> pWeakDemuxer(pDemuxer_);
  // std::function<void(double)> demuxer_seek_func = [pWeakDemuxer] (double
  // percent) {
  //   auto pSharedDemuxer = pWeakDemuxer.lock();
  //   if(pSharedDemuxer == nullptr) {
  //     LOGWARN("Demuxer has been deconstruct");
  //   }
  //   uint64_t seek_pts = pSharedDemuxer->Seek(percent);
  //   MessageBus::Notify("Decoder:seek_pts", seek_pts);
  // };
  // MessageBus::Attach(demuxer_seek_func, this, "");
  return 0;
}

AVCodecParameters* DemuxThread::GetVideoCodecPar() {
  return pDemuxer_->GetStreamCodecPar(pDemuxer_->GetVideoStreamIndex());
}

AVRational* DemuxThread::GetVideoFrameRate()
{
	return pDemuxer_->GetFrameRate();
}

double DemuxThread::GetVideoFrameRateDiv()
{
	return pDemuxer_->GetFrameRateDiv();
}

AVRational* DemuxThread::GetVideoTimeBase()
{
	return pDemuxer_->GetTimeBase();
}

AVRational* DemuxThread::GetAudioTimeBase()
{
	return pDemuxer_->GetAudioTimeBase();
}

AVCodecParameters* DemuxThread::GetAudioCodecPar() {
  return pDemuxer_->GetStreamCodecPar(pDemuxer_->GetAudioStreamIndex());
}

int DemuxThread::Seek(double percent) {
  LOGERROR("Thread %s Seek is not implemented", GetName().c_str());
  return 0;
}

int DemuxThread::RunOnce() {
  LOGONTEST("Thread %s  RunOnce", GetName().c_str());
  // 如果在倒放状态，且不应该往解码队列中推送流和帧，则不进行解码
  if(reverse && !can_push_Video && !can_push_Audio) {
      return 0;
  }

  if(reverse && (can_push_Video || can_push_Audio)) {
      if (be_notified_counter == 2)
      {
          notified = false;
        be_notified_counter = 0;
		MessageBus::Notify("videodecoder_can_decode_Frame");
		MessageBus::Notify("audiodecoder_can_decode_Frame");
      }
      if (notified) return 0;
  }
  // 如果有超时的数据需要push，则先push。push失败直接返回
  if (pPkt_ != nullptr) {
    if (pPkt_->GetIndex() == nVideo_index_ && (!reverse | can_push_Video))
		if( pVideo_packet_queue_->push_timeout(pPkt_, 25) == false) return 0;
    if (pPkt_->GetIndex() == nAudio_index_ && (!reverse | can_push_Audio))
		if( pAudio_packet_queue_->push_timeout(pPkt_, 100) == false) return 0;

    check_reverse_pos(pPkt_->GetPts(),pPkt_->GetIndex());
    pPkt_ = nullptr;
	return 0; // 避免reverse检查通过但是RunOnce再读下一帧
  }
  pPkt_ = std::make_shared<Packet>();
  int ret = pDemuxer_->Read(pPkt_);
  if (ret == 1 && !reverse) {
    LOGIMPT("Thread %s no more packet", GetName().c_str());
    pVideo_packet_queue_->push(nullptr);
    pAudio_packet_queue_->push(nullptr);
    ThreadEventLoop::Finish();
  }
  if (ret == 1 && reverse) {
      check_reverse_pos(3600000000, 0);
      check_reverse_pos(3600000000, 1);
      pPkt_ = nullptr;
      return 0;
  }
  LOGONTEST("Thread %s packet index = %d", GetName().c_str(),
            pPkt_->GetIndex());
  bool retbool = check_reverse_pos(pPkt_->GetPts(),pPkt_->GetIndex());
  if (retbool)
  {
      pPkt_ = nullptr;
      return 0;
  }
  if (pPkt_->GetIndex() == nVideo_index_ && (!reverse || can_push_Video)) {
    // LOGONTEST("Thread %s push video packet to queue", GetName().c_str());
    // LOGONTEST("Thread %s video packet queue, queue size = %u, capacity = %u",
    //           GetName().c_str(), pVideo_packet_queue_->size(),
    //           pVideo_packet_queue_->capacity());
    //LOGIMPT("Video Pkt Pos:%lld", pPkt_->GetPts());
	if( pVideo_packet_queue_->push_timeout(pPkt_, 25) == false)
	{
      LOGWARN("Thread %s video packet queue, queue size = %u, capacity = %u",
              GetName().c_str(), pAudio_packet_queue_->size(),
              pAudio_packet_queue_->capacity());
      LOGWARN("Thread %s try to push video packet queue timeout",
              GetName().c_str());
      return 0;
	}
  }
  if (pPkt_->GetIndex() == nAudio_index_ && (!reverse || can_push_Audio)) {
    //LOGIMPT("Audio Pkt Pos:%lld", pPkt_->GetPts());
    // LOGONTEST("Thread %s push audio packet to queue", GetName().c_str());
    // LOGONTEST("Thread %s audio packet queue, queue size = %u, capacity = %u",
    //           GetName().c_str(), pAudio_packet_queue_->size(),
    //           pAudio_packet_queue_->capacity());
	if( pAudio_packet_queue_->push_timeout(pPkt_, 25) == false)
	{
      // LOGWARN("Thread %s audio packet queue, queue size = %u, capacity = %u",
      //         GetName().c_str(), pAudio_packet_queue_->size(),
      //         pAudio_packet_queue_->capacity());
      // LOGWARN("Thread %s try to push audio packet queue timeout",
      //         GetName().c_str());
      return 0;
    }
  }
  pPkt_ = nullptr;
  return 0;
}

int DemuxThread::ClearImpl() {
  pPkt_ = nullptr;
  pDemuxer_->Clear();
  pVideo_packet_queue_->clear();
  pAudio_packet_queue_->clear();
  return 0;
}

inline bool DemuxThread::check_reverse_pos(int64_t pos, int PktIndex) {
    if(reverse && pos>=next_IFrame_pos){
        if(PktIndex==nVideo_index_)
			can_push_Video = false;
        if (PktIndex==nAudio_index_)
			can_push_Audio = false;

		if(can_push_Audio || can_push_Video) return true;
        if (notified) return true;

        notified = true;
        be_notified_counter = 0;
		MessageBus::Notify("audiodecoder_notify_last_packet",1);
		MessageBus::Notify("videodecoder_notify_last_packet",1);

		reverse_origin_pos = next_IFrame_pos = reverse_seeked_pos;
		pDemuxer_->SeekByPos(reverse_seeked_pos-300);
//		pDemuxer_->SeekByOriginPts(reverse_seeked_origin_pts-20);
        pPkt_ = std::make_shared<Packet>();
        pDemuxer_->Read(pPkt_);
        reverse_seeked_pos = pPkt_->GetPts();
		reverse_seeked_origin_pts = pPkt_->GetOriginPts();
		pDemuxer_->SeekByPos(reverse_seeked_pos);
        return true;
    }
    return false;
}

void DemuxThread::toggleReverse(int64_t pos){
    reverse = !reverse;
	//reverse_origin_pos=pos;

	if(!pos)
	{
		reverse_seeked_pos = next_IFrame_pos = 0;
		can_push_Video = can_push_Audio = false;
		return;
	}
    notified = false;
    be_notified_counter = 0;
    can_push_Audio = can_push_Video = true;
    pPkt_ = std::make_shared<Packet>();
    pDemuxer_->SeekForwardByPos(pos);
    pDemuxer_->Read(pPkt_);
    next_IFrame_pos = pPkt_->GetPts();
    pPkt_ = nullptr;

    reverse_origin_pos = pos;

    pPkt_ = std::make_shared<Packet>();
    pDemuxer_->SeekByPos(pos);
    pDemuxer_->Read(pPkt_);
    reverse_seeked_pos = pPkt_->GetPts();
	reverse_seeked_origin_pts = pPkt_->GetOriginPts();

    can_push_Video = can_push_Audio = true;
    pDemuxer_->SeekByPos(pos);
}

void DemuxThread::toggle_can_push(int x) {
    if(!x) can_push_Video = true;
    else  can_push_Audio = true;
    be_notified_counter = can_push_Audio + can_push_Video;
}

}  // namespace AV
