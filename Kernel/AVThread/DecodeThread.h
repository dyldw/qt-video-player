#pragma once
#include "Common.h"
#include <stdint.h>

namespace AV {

class DecodeThread final : public ThreadEventLoop {
 public:
  DecodeThread();
  ~DecodeThread();
  int Init(AVCodecParameters* codecPar,
           std::shared_ptr<PacketQueue> pInput_packet_queue,
           std::shared_ptr<FrameQueue> pOutput_frame_queue);
  void SetFrameRate(AVRational* framerate);
  void SetTimeBase(AVRational* framerate);
  void toggleReverse(int64_t pos);
  void notify_last_packet(int x);
  void toggle_EndofPacket(int x);

 protected:
  virtual int RunOnce() override;
  friend MediaPlayer;

 private:
  virtual int ClearImpl() override;

 protected:
  FramePtr pFrame_;  // 没有放入帧队列的帧
  std::shared_ptr<Decoder> pDecoder_;
  std::shared_ptr<PacketQueue> pInput_packet_queue_;
  std::shared_ptr<FrameQueue> pOutput_frame_queue_;

 private:
  bool reverse{false};  // 倒放模式
//bool can_pop{false};  //
  bool End_of_Packet{false};
  bool notified{false};
  int64_t End_pos;
  AVRational* Framerate;
  AVRational* time_base;
};
}  // namespace AV
