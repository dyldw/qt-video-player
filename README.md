转移到 [开发文档合集](https://bytedance.feishu.cn/docx/doxcnn5w8u2qRYVYKPAGQb07qIf?from=from_copylink)

# 介绍

[TOC]

## 合作开发相关
本地分支 name/feature  --> 远程分支 name/feature --> PullRrquest --> CodeReview --> master

name是自己的名字, feature是这个大概是干什么的。

一个功能一个分支。提出PR后需要找人进行代码评审，任意一人通过就可以合并了。
（大家都是管理员权限，想怎么评审改就怎么改就行）

注意及时更新同步代码。

代码风格统一使用clang-format吧，选择google-style
`clang-format -style=google -dump-config > .clang-format`

App部分的代码还需要格式化一下

每个人负责的模块可以建一个doc目录，简要介绍大概是个什么样子。也可以写在[开发文档](https://bytedance.feishu.cn/docx/doxcnn5w8u2qRYVYKPAGQb07qIf#doxcnacGKuqWYYkQo0yVesdo41g)合集里面

希望有人可以集成一下doxygen，用来生成文档

## 文件组织
目前的文件组织还将会有重大调整，只是临时这样子

App和Kernel还需要拆分很多
## 编译
```shell
autogen.bat
```
注意事项：
1. 需要安装visual studio，需要安装qt
2. 需要设置环境变量 `QTDIR624_64 = D:\Qt\6.2.4\msvc2019_64\lib\cmake`
3. 目前是硬编码要播放的视频路径在`App\QtPlayer\sdlchild\sdlchild.cpp:27`，编译前需要写成自己的视频路径。测试内核的话默认视频路径硬编码在`Kernel\Test\main.cpp:27`，可以通过命令行传入指定的要播放的视频。
4. SDL ffmpeg

## 视频解码——ffmpeg

参考：https://github.com/redknotmiaoyuqiao/MMPlayer 

### 整体流程

![img](README.assets/cos-file-urlurl=https%253A%252F%252Fkm-pro-1258638997.cos.ap-guangzhou.myqcloud.com%252Ffiles%252Fphotos%252Fpictures%252F202008%252F1596532796_69_w356_h807.png&is_redirect=1)

## 整体架构设计
```shell
视频路径 --> 播放器内核（音视频播放主流程）--> 音视频设备
               ^
               |
数据资源 --> 播放器上层 （播放器其他逻辑） 
               ^
               |
              用户 
```

## 分工
播放器前端开发：高怀基

播放器后端开发：董一龙，于承业，方得丞