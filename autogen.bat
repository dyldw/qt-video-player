@REM 
@REM QtMediaPlayer(app) & MediaPlayerTest(kernel) & QtPlayer(front end) Build script for Win32 platform
@REM 
@REM You'll need Visual C++ installed to compile - also execute the 
@REM "vcvars32.bat" in VC install directotry before running this one.
@REM 
@REM Copyright (c) yilongdong
@REM

@if "%DevEnvDir%"=="" goto nodevdir

cmake -B Build

@REM 播放内核
@REM devenv Build\Kernel\AVKernel.sln /Build "Debug|x64"
devenv Build\Kernel\AVKernel.sln /Build "Release|x64"

@REM 播放前端
@REM devenv Build\App\QtPlayer.sln /Build "Debug|x64"
@REM devenv Build\App\QtPlayer.sln /Build "Release|x64"

@REM 整个项目
@REM devenv Build\QtMediaPlayer.sln /Build "Debug|x64"
devenv Build\QtMediaPlayer.sln /Build "Release|x64"

@REM Build\App\Release\QtFrontEnd.exe

@goto end


:nodevdir

@echo off
echo ******************************************************************************************
echo ** "                                                                                     "
echo ** "ERROR: Visual Studio path not set.                                                   "
echo ** "                                                                                     "
echo ** "Open 'tools'->'Developer Command Prompt' from Visual Studio IDE, or set devenv path. "
echo ** "e.g. Set ENV_PATH = xxx\Microsoft Visual Studio\xx\Community\Common7\IDE\devenv.exe, "
echo ** "then try again.                                                                      "
echo ** "                                                                                     "
echo ******************************************************************************************
echo end

:end